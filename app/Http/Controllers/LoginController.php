<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {
    $this->middleware(['guest']);
    }

    public function index()
    {
        return view('login');
    }

    public function login(Request $req)
    {
        $this->validate($req, [

            
            'email' => 'required|email',
          
            'password' => 'required',
        ]);

        if(! auth()->attempt($req->only('email', 'password')))
        {
                return back()->with('status', "Invalid Login Credentials");
        }

        $usertype= Auth::user()->usertype;
    if($usertype=='1') {
        return view('adminDashboard.main');
        }
        else {
            return redirect()->route('my-profile');
        }
        
    }
}
