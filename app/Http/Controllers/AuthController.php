<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Auth;
class AuthController extends Controller
{

    public function __construct()
    {
    $this->middleware(['guest']);
    }


    public function signup()
    {
        return view('signup');
    }

 

    public function userRegister(Request $req)
    {
        $this->validate($req, [

            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed',
        ]);

        // create new user    //
        
        $user = new User();
        $user ->name = $req->name;
        $user ->email = $req->email;
        $user ->password = bcrypt($req->password);

        $user->save();
        
        auth()->attempt($req->only('email', 'password'));

        $usertype= Auth::user()->usertype;
        if($usertype=='1') {
            return view('adminDashboard.main');
            }
            else {
                return redirect()->route('my-profile');
            }
    }
}
