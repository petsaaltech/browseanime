<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminBlogPost;
use App\Models\AdminCategoryBlog;
use Illuminate\Support\Facades\Storage;

class BlogPostController extends Controller
{
    public function adminBlog()
    {
        return view("adminDashboard.blog");
    }


    public function index()
    {
        if (request()->wantsJson()) {
            return response(
                AdminBlogPost::all()
            );
        }
        $adminblogposts= AdminBlogPost::paginate(10);
        $admincategoryblogs = AdminCategoryBlog::all();
        return view('adminDashboard/adminblogpost.index',compact('adminblogposts','admincategoryblogs'));
    }


    public function store(Request $request)
    {
        // return $request;
        
        $image_path = '';

        if ($request->hasFile('blog_image')) {
            $image_path = $request->file('blog_image')->store('AdminBlogPost', 'public');
        }

        $adminblogpost = AdminBlogPost::create([
            'blog_title' => $request->blog_title,
            'blog_description' => $request->blog_description,
            'blog' => $request->blog,
            'blog_image' => $image_path,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'focus_keywords' => $request->focus_keywords,
            'blog_slug' => $request->blog_slug,
            'blog_post_category_id' => $request->blog_post_category_id,
            'hash_tags' => $request->hash_tags,
            'blog_post_views' => $request->blog_post_views,

        ]);

       

        if (!$adminblogpost) {
            return redirect()->back()->with('error', 'Sorry, there a problem while creating Blog.');
        }
        return redirect()->back()->with('success', 'Success, Your Blog have been created.');
    }

    public function update(Request $request, AdminBlogPost $adminblogpost)
    {
        // return $product;
        $adminblogpost->blog_title = $request->blog_title;
        $adminblogpost->blog_description = $request->blog_description;
        $adminblogpost->blog = $request->blog;
        $adminblogpost->meta_title = $request->meta_title;
        $adminblogpost->meta_description = $request->meta_description;

        $adminblogpost->focus_keywords = $request->focus_keywords;
        $adminblogpost->blog_slug = $request->blog_slug;
        $adminblogpost->blog_post_category_id = $request->blog_post_category_id;
        $adminblogpost->hash_tags = $request->hash_tags;
        $adminblogpost->blog_post_views = $request->blog_post_views;


        if ($request->hasFile('blog_image')) {
            // Delete old image
            if ($adminblogpost->blog_image) {
                Storage::delete($adminblogpost->blog_image);
            }
            // Store image
            $image_path = $request->file('blog_image')->store('AdminBlogPost', 'public');
            // Save to Database
            $adminblogpost->blog_image = $image_path;
        }

        if (!$adminblogpost->save()) {
            return redirect()->back()->with('error', 'Sorry, there\'re a problem while updating Blog.');
        }
        return redirect()->back()->with('success', 'Success, your Blog have been updated.');
    }

    public function destroy(AdminBlogPost $adminblogpost)
    {
        if ($adminblogpost->blog_image) {
            Storage::delete($adminblogpost->blog_image);
        }

        $adminblogpost->delete();

       return response()->json([
           'success' => true
       ]);
    }
}
