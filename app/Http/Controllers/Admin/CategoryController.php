<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminCategoryBlog;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        if (request()->wantsJson()) {
            return response(
                AdminCategoryBlog::all()
            );
        }
        $adminblogcategorys = AdminCategoryBlog::paginate(10);
        return view('adminDashboard/adminblogcategory.newindex',compact('adminblogcategorys'));
    }

  
    public function create()
    {
        
        // return view('category.create');
    }

   
    public function store(Request $request)
    {
      
        //    $request->validate(['name' => 'required|unique:admincategoryblog,name']);
        //    if($request->status)
        //    {
        //         $status = 1;
        //    }
        //    else
        //    {
        //     $status = 0;
        //    }
     
        $result = AdminCategoryBlog::create([
            'name' => $request->name
        ]);

       

        if (!$result) {
            return redirect()->back()->with('error', 'Sorry, there a problem while creating Blog.');
        }
        return redirect()->back()->with('success', 'Success, Your Blog have been created.');
    }

    public function show(AdminCategoryBlog $category)
    {
        //
    }

    public function edit(AdminCategoryBlog $category)
    {
        // return view('category.create',compact('category'));
    }
    public function update(Request $request, AdminCategoryBlog $adminblogcategory)
    {
        
        $adminblogcategory->name = $request->name;
        if (!$adminblogcategory->save()) {
            return redirect()->back()->with('error', 'Sorry, there\'re a problem while updating Blog.');
        }
        return redirect()->back()->with('success', 'Success, your Blog have been updated.');
    }

    public function destroy(AdminCategoryBlog $adminblogcategory)
    {
        $adminblogcategory->delete();

        return response()->json([
            'success' => true
        ]); 
    }
}
