<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Anime;
use App\Models\Category;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Auth;
use App\Models\AnimeReview;
use App\Models\AdminBlogPost;
use App\Models\AnimeSeach;
class AdminController extends Controller
{
    public function loginIndex()
    {
        
        {
            return view('adminDashboard.login');
        }
        
        
    }


    public function login()
    {
        return view('adminDashboard.main');
    }

    public function dashboard()

    {
        $users= User::all();
        $reviews=AnimeReview::all();
        $animesearches= AnimeSeach::all();
        $animes= Anime::all();
        $adminblogposts= AdminBlogPost::all();
        $usertype= Auth::user()->usertype;
        if ($usertype=='1'){
            return view('adminDashboard.main',compact('adminblogposts','animes','animesearches','reviews','users'));
        }
        else {
            return redirect('login');
        }
        
    }

    public function logout()
    {
        auth()->logout();

        return view('signup');
    }
}
