<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Anime;

class SitemapController extends Controller
{
    public function index($value='')
    {
        $animes = Anime::latest()->get();
  
        return response()->view('sitemap', [
            'animes' => $animes
        ])->header('Content-Type', 'text/xml');
    }
    
}
