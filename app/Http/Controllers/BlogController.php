<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\BlogCategory;
use App\Models\AdminBlogPost;

class BlogController extends Controller
{
    // public function __construct()
    // {
    // $this->middleware(['auth']);
    // }

    
    public function addBlog()
    {
        return view("adminDashboard.blog");

       
    }


    public function addBlogCategory()
    {
        return view("adminDashboard.blogCategory");

    }

    public function storeBlogCategory(Request $request)
    {


        // validations

        $validateData  = $request->validate([

            'title' => 'required',
            'description' => 'required',
            ]);


        // store dataabase table

        $data = new BlogCategory();

        $data->	cat_title = $request->title;
        $data->	cat_des = $request->description; 
       
        $data->save();


        // return redirect()->route('viewBlogCategory');

        return redirect()->route('viewBlogCategory')->with('status', 'Review Updated!');

    }

    public function viewBlogCategory()
    {
        return view("adminDashboard.blogCategoryView");

    }


    public function blogpost()
    {
        $adminblogposts= AdminBlogPost::all();
        return view('blogpostpage',compact('adminblogposts'));
    }

    public function blogpostdetail($slug)
    {
        $adminblogposts = AdminBlogPost::where('blog_slug',$slug)->increment('blog_post_views');
        $adminblogpost = AdminBlogPost::updateOrCreate(
            ['blog_slug' =>  $slug]
        );
        // return $slug;
        $adminblogposts = AdminBlogPost::where('blog_slug',$slug)->first();
        return view('blogpostdetailpage',compact('adminblogposts'));
    }
}
