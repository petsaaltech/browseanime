<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'blog_title', 
        'longText',
        'blog_featured_image',
        'meta_title',
        'meta_des',
        'meta_keywords',
        'blog_slug',
        'blog_category',
        'blog_hash_tags',
        'blog_status',
        'blog_post_views', 

    ];
 
}
