<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminBlogPost extends Model
{
    use HasFactory;

    protected $fillable =[
        'blog_title',
        'blog_description',
        'blog',
        'blog_image',
        'meta_title',
        'meta_description',
        'focus_keywords',
        'blog_slug',
        'blog_post_category_id',
        'hash_tags',
        'blog_post_views'
    ];
}
