<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate([
            'email' => 'admin@browseanime.com'
        ], [
            'name' => 'Admin',
            'usertype' => '1',
            'email'=>'admin@browseanime.com',
            'password' => bcrypt('browseanime#0123')
        ]);
    }
    
}
