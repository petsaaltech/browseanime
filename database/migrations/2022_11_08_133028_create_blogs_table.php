<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('blog_title');
            $table->string('longText');
            $table->string('blog_featured_image');
            $table->string('meta_title');
            $table->string('meta_des');
            $table->string('meta_keywords');
            $table->string('blog_slug');
            $table->string('blog_category');
            $table->string('blog_hash_tags');
            $table->string('blog_status');
            $table->string('blog_post_views');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
