<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToAdminBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_blog_posts', function (Blueprint $table) {
            $table->string('focus_keywords')->after('meta_description');
            $table->string('blog_slug');
            $table->string('blog_post_category_id');
            $table->string('hash_tags');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_blog_posts', function (Blueprint $table) {
            //
        });
    }
}
