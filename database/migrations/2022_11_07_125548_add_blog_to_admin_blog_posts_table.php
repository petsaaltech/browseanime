<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBlogToAdminBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_blog_posts', function (Blueprint $table) {
            $table->string('blog_title')->after('id');
            $table->longText('blog_description')->nullable();
            $table->longText('blog')->nullable();
            $table->string('blog_image')->nullable();
            $table->string('meta_title');
            $table->longText('meta_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_blog_posts', function (Blueprint $table) {
            //
        });
    }
}
