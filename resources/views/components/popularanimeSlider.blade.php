<div class="container-fluid p-0" style="position: initial;">
    <div class="swiper mySwiper">
        <div class="swiper-wrapper">


        @foreach($popular_anime as $key => $d)
@if(isset($popular_anime)) 
            <div class="swiper-slide">
                <div class="swiper-img-box">
                    <div class="hover-img-size position-relative">
                        <a href="{{ route('animeDetail' , $d->anime_id) }}">
                            <img src="{{$d->anime_picture}}" alt="" />
                        </a>
                        <div class="hover-img-description">
                            <div>
                                <div class="d-flex justify-content-end m-2">
                                    <!-- <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white"
                                        class="bi bi-bookmark-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z" />
                                    </svg> -->
                                </div>
                                <div class="mt-5">
                                    @if($d->english_title == null)
                                    <a href="{{ route('animeDetail' , $d->anime_id)  }}">
                                    <p class="white-color m-2">{!! Str::limit($d->japanese_title, 30) !!}</p>
                                     </a>
                                    @else
                                    <a href="{{ route('animeDetail' , $d->anime_id)  }}">
                                    <p class="white-color m-2">{{ Str::limit($d->english_title, 50) }}</p>
                                    </a>

                                    @endif
                                    <p class="white-color m-2 mx-2">
                                        <span>2017</span> . <span>3 seasons</span> .
                                        <span>Netflix</span>
                                    </p>
                                    @if(auth()->user())

                                    <div class="d-flex">
                                        <div>
                                            <p class="mb-0 btn-hover-fade mx-2">trailer</p>
                                        </div>
                                        <div>
                                            <select class="form-select mb-0 btn-hover-fade mx-2"
                                                aria-label="Default select example">
                                                <option selected>Watch Options</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                    </div>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- @if($d->english_title == null)

                    <p class="title-text">{!! Str::limit($d->japanese_title, 30) !!}</p>

                    @else
                    <p class="title-text">{{ Str::limit($d->english_title, 50) }}</p>
                    @endif -->
                </div>
            </div>

            @endif
            @endforeach

        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    
</div>