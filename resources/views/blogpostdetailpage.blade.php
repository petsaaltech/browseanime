@extends('main')
@section('meta-description', "We recommend $adminblogposts->meta_description")
@section('meta-keywords', "$adminblogposts->focus_keywords anime")

@section('title', "Watch $adminblogposts->blog_title")


@section('content')



<!-- review featured banner image -->

<div class="container mt-5 mb-5">

    <div class="row img-row">


        <img src="{{ Storage::url($adminblogposts->blog_image) }}" alt="" />


    </div>

</div>

<!-- review featured banner image -->
<div class="container review-section extra-padding-container" style="background: #EEEEEE;">
    <div class="row">
        <div class="col-md-12">
            <h1 class="review-section-title">
			    {{$adminblogposts->blog_title}}
            </h1>
            <h5 class="review-section-username" style="font-size: 1.05rem;">    
            Published : {{Carbon\Carbon::parse($adminblogposts->created_at)->isoFormat('D-MMM-Y')}} 
               <i class=" fa fa-eye" style="margin-left: 40px;"> {{$adminblogposts->blog_post_views}}</i> 
            
		    </h5>

            <div>
                <p>
                {!!$adminblogposts->blog!!}
                </p>
            </div>
        </div>
    </div>
</div>



<!-- Reviews Listing Grid -->


<!-- Reviews Listing Grid -->


@endsection