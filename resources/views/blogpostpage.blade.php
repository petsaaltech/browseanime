@extends('main')
@section('meta-description', 'Anime Recommendations, Anime Episodes, Anime wishlist, Anime track, Anime wallpapers,
Famous Anime Characters & Much More')
@section('meta-keywords', 'anime, anime recommendations, anime record, anime track, anime characters, upcoming anime,
popular anime')

@section('title', 'Blog Post, Recommendations & Much More')


@section('content')

<div class="container">
    <div class="row">
        @foreach ($adminblogposts as $key =>$adminblogpost)
        <div class="col-md-4  my-3">
            <div class="card" style="box-shadow: rgb(0 0 0 / 20%)0 60px 40px -7px;">
                <img src="{{ Storage::url($adminblogpost->blog_image) }}" class="card-img-top"
                    alt="Hollywood Sign on The Hill" />
                <div class="card-body">
                    <h5 class="card-title">{{$adminblogpost->blog_title}}</h5>
                    <p class="card-text"><small class="text-muted">Published :
                            {{Carbon\Carbon::parse($adminblogpost->created_at)->isoFormat('D-MMM-Y')}} </small></p>
                    <!-- <p class="card-text">
                {{$adminblogpost->blog_description}}
                </p> -->

                    <div class="d-flex" style="display: flex;justify-content: space-around;width: 75%;">
                        <div class="col-lg-5 col-5">

                            <a class="btn anime-views-count" style="color: white;background: #54229E;"
                                href="{{route('blog-detail',[$adminblogpost->blog_slug])}}">Read Now</a>
                        </div>
                        <div class="col-lg-5 col-5">

                            <p class=" anime-views-count" style="color: white;background: #54229E;"> <i
                                    class="fa fa-eye"></i>
                                {{$adminblogpost->blog_post_views}}

                                views</p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endforeach

    </div>


</div>


@endsection