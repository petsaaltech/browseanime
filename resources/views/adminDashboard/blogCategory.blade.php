@extends('adminDashboard.master')

@section('adminMainContent')


<div class="container p-5 mt-4">
        <div class="row mt-5">
            <div class="col-lg-12">
                <h2 class="h1-font-size">Create a Blog Category</h2>
                <hr>
            </div>
 
</div>

        </div>
    </div>

    <div class="container ">
        <div class="row">
            <div class="col-lg-8 offset-md-2">
                <form action="{{ route('storeBlogCategory')  }}" method="POST">
                    @csrf
                    
                    <div class="row">


                    <div class="col-lg-12 mb-3">
                        <label for="title" class="form-label">Blog Category Title</label>
                        <input type="text" class="form-control" id="title"
                        value="{{ old('title') }}"
                        name="title" placeholder="For Example: News, Reviews, Upcoming Events">
                        @error('title') {{$message}} @enderror
                    </div>

                    <div class="col-lg-12 mb-3">
                        <label for="blogCategoryDescription" name="blogCategoryDescription" class="form-label">Blog Category Description</label>
                        <textarea class="form-control" id="blogCategoryDescription" 
                        name="description"  rows="3" 
                        placeholder="Type something about this category "></textarea>
                        @error('description') {{$message}} @enderror

                    </div>
 
                    <div class="col-lg-12">
                        <hr>
                    </div>
 
                   
 
                     
                   
                    <div class="col-lg-12 mt-5 mb-6">
                        <button type="submit"  class="btn btn-dark">Create Blog Category</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
