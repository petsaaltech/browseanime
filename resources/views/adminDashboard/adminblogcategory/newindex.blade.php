@extends('adminDashboard.master')

@section('adminMainContent')

@section('css')
<link rel="stylesheet" href="{{ asset('sweetalert2/sweetalert2.min.css') }}">

@endsection
@section('adminMainContent')

<div class="container my-5">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12" style="margin-left: 130px;">

                <div class="card product-list">
                    <div class="card-header">
                        <h4 style="float: left; margin-top:25px">Admin Blog Post Category</h4>
                        <a href="#" style="float: right;width: 260px;height: 50px;margin-top: 10px;" class="btn btn-primary"
                            data-toggle="modal" data-target="#addproduct">
                            <i class="fa fa-plus-circle fa-lg"></i> Add New Category</a>
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-left">
                            <thead style="text-align: center;">
                                <tr>
                                    <th>ID</th>
                                    <th>Blog Post Category Name</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center;">
                                @foreach ($adminblogcategorys as $key =>$adminblogcategory)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$adminblogcategory->name}}</td>
                                    <td>{{Carbon\Carbon::parse($adminblogcategory->created_at)->isoFormat('D-MMM-Y')}}</td>
                                    <td>{{Carbon\Carbon::parse($adminblogcategory->updated_at)->isoFormat('D-MMM-Y')}}</td>
                                    <td>    
                                        <a href="#" class="btn btn-outline-info btn-sm btn-rounded" data-toggle="modal"
                                            data-target="#editcategory{{$adminblogcategory->id}}"><i class="fa fa-edit"></i>
                                            Edit</a>

                                        <button class="btn btn-outline-danger btn-sm btn-rounded btn-delete"
                                            data-url="{{route('adminblogcategory.destroy',$adminblogcategory)}}"><i
                                                class="fas fa-trash"></i></button> 


                                    </td>
                                </tr>

                                <!-- edit supplier model -->
                                <div class="modal right fade" id="editcategory{{$adminblogcategory->id}}" data-backdrop="static"
                                    tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document" style="margin-right: 130px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="staticBackdropLabel">Edit Category</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$adminblogcategory->id}}
                                            </div>
                                            <div class="modal-body">

                                                <form action="{{ route('adminblogcategory.update',$adminblogcategory) }}" method="POST"
                                                    enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="form-group">
                                                        <label for="name">Category Name</label>
                                                        <input type="text" name="name"
                                                            class="form-control @error('name') is-invalid @enderror"
                                                            id="name" placeholder="Name"
                                                            value="{{ old('name',$adminblogcategory->name) }}">
                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>


                                                    <button class="btn btn-warning btn-block" type="submit">Update</button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </tbody>
                        </table>
                        {{ $adminblogcategorys->render() }}

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- add supplier model -->

<div class="modal right fade" id="addproduct" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="margin-right: 130px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="staticBackdropLabel">Add Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{ route('adminblogcategory.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="name">Category Name</label>
                        <input required type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                            id="name" placeholder="Name" value="{{ old('name') }}">
                        @error('full_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <button class="btn btn-primary btn-block" type="submit">Create</button>
                </form>

            </div>
        </div>
    </div>
</div>



<style>
.modal.right .modal-dialog {
    /* position: absolute; */
    top: 0;
    right: 0;
    margin-right: 14.5vh;
}

.modal.fade:not(.in).right .modal-dialog {

    -webkit-transform: translate3d(25%, 0, 0);
    transform: translate3d(25%, 0, 0, 0);
}
</style>

<script src="{{ asset('sweetalert2/sweetalert2.all.min.js') }}"></script>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<script>
$(document).ready(function() {
    $(document).on('click', '.btn-delete', function() {
        $this = $(this);
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "Do you really want to delete this product?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.post($this.data('url'), {
                    _method: 'DELETE',
                    _token: '{{csrf_token()}}'
                }, function(res) {
                    $this.closest('tr').fadeOut(500, function() {
                        $(this).remove();
                    })
                })
            }
        })
    })
});

$(document).ready(function() {
    bsCustomFileInput.init();
});
</script>

@endsection

