<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">


            <div class="container">
                <div class="row my-5 ms-10">
				<div class="col-md-2 col-sm-6 ">
                        <div class="counter red" >
                            <div class="counter-icon">
                                <i class="fas fa-user " style="color:#EC294B"></i>
                            </div>
                            <h3>Users</h3>
                            <span class="counter-value" >{{$users->count()}}</span>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6 ms-14">
                        <div class="counter" >
                            <div class="counter-icon">
                                <i class="fas fa-spinner " style="color:#54229E"></i>
                            </div>
                            <h3>Animes</h3>
                            <span class="counter-value" >{{$animes->count()}}</span>
                        </div>
                    </div>
					<div class="col-md-2 col-sm-6 ms-14">
                        <div class="counter green">
                            <div class="counter-icon">
                                <i class="fas fa-blog" style="color:#03BFB0"></i>
                            </div>
                            <h3>Blog Posts</h3>
                            <span class="counter-value">{{$adminblogposts->count()}}</span>
                        </div>
                    </div>
					<div class="col-md-2 col-sm-6 ms-14">
                        <div class="counter orange" >
                            <div class="counter-icon">
                                <i class="fa fa-list-alt" style="color:#FB9A00"></i>
                            </div>
                            <h3>reviews</h3>
                            <span class="counter-value">{{$reviews->count()}}</span>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 ms-14">
                        <div class="counter blue">
                            <div class="counter-icon">
                                <i class="fa fa-search" style="color:#0092CD"></i>
                            </div>
                            <h3>Anime Searches</h3>
                            <span class="counter-value">{{$animesearches->count()}}</span>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!--end::Content-->
    </div>
    <!--end::Content wrapper-->
    <!--begin::Footer-->
    @include('adminDashboard.layouts.footer')
    <!--end::Footer-->
</div>


<style>
.counter {
    color: #54229E;
    background: linear-gradient(to right bottom, #fff 50%, #f9f9f9 51%);
    font-family: 'Comfortaa', cursive;
    text-align: center;
    width: 200px;
    padding: 20px 0 0;
    margin: 0 auto;
    border-radius: 10px 10px;
    box-shadow: 0 0 15px -5px rgba(0, 0, 0, 0.3);
    overflow: hidden;
}

.counter .counter-icon {
    font-size: 45px;
    margin: 0 0 10px;
}

.counter h3 {
    font-size: 18px;
    font-weight: 700;
    text-transform: capitalize;
    margin: 0 0 20px;
}

.counter .counter-value {
    color: #fff;
    background: linear-gradient(to right bottom, #5d21b7, #54229E);
    font-size: 40px;
    font-weight: 700;
    line-height: 40px;
    padding: 7px 0 3px;
    display: block;
}

.counter.blue {
    color: #0092CD;
}

.counter.blue .counter-value {
    background: linear-gradient(to right bottom, #06BBF4, #0092CD);
}

.counter.red{
	color: #EC294B;
}
.counter.red .counter-value {
	
    background: linear-gradient(to right bottom, #f71d41, #EC294B);
}

.counter.orange {
    color: #FB9A00;
}

.counter.orange .counter-value {
    background: linear-gradient(to right bottom, #FCCA09, #FB9A00);
}

.counter.green {
    color: #03BFB0;
}

.counter.purple {
    color: #683BAB;
}

.counter.green .counter-value {
    background: linear-gradient(to right bottom, #00E2CD, #03BFB0);
}

i[class]{
	font-size: 3rem;
}

@media screen and (max-width:990px) {
    .counter {
        margin-bottom: 40px;
    }
}
</style>

<!-- <script>
	 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
 
 $(document).ready(function(){
	 $('.counter-value').each(function(){
		 $(this).prop('Counter',0).animate({
			 Counter: $(this).text()
		 },{
			 duration: 3500,
			 easing: 'swing',
			 step: function (now){
				 $(this).text(Math.ceil(now));
			 }
		 });
	 });
 });
</script> -->