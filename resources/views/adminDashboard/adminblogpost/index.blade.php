@extends('adminDashboard.master')

@section('adminMainContent')

@section('css')
<link rel="stylesheet" href="{{ asset('sweetalert2/sweetalert2.min.css') }}">

@endsection
@section('adminMainContent')

<div class="container my-5">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12" style="margin-left: 65px;">

                <div class="card product-list" style="display: flex; width: fit-content;">
                    <div class="card-header">
                        <h4 style="float: left; margin-top:25px">Admin Blog Post</h4>
                        <a href="#" style="float: right; width: 250px;height: 50px;margin-top: 10px;" class="btn btn-primary" data-toggle="modal"
                            data-target="#addproduct">
                            <i class="fa fa-plus-circle fa-lg"></i>Add New Admin Blog Post </a>
                    </div>
                    <div class="card-body">

                        <table class=" table-bordered table-left">
                            <thead style="text-align: center;">
                                <tr>
                                    <th>ID</th>
                                    <th>Blog Title</th>
                                    <th>Blog Image</th>
                                    <!-- <th>Meta Title</th>
                                    <th>Focused Keywords</th>
                                    <th>Meta Description</th>
                                    <th>Blog Slug</th>
                                    <th>Blog Post Category</th>
                                    <th>Hash Tags</th>
                                    <th>Blog Post Views</th> -->
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center;">
                                @foreach ($adminblogposts as $key =>$adminblogpost)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$adminblogpost->blog_title}}</td>
                                    <!-- <td>{{$adminblogpost->blog_description}}</td> -->
                                    <!-- <td>{!!$adminblogpost->blog!!}</td> -->
                                    <td><img class="product-img" src="{{ Storage::url($adminblogpost->blog_image) }}"
                                            alt="" style="width:50%"></td>
                                    <!-- <td>{{$adminblogpost->meta_title}}</td> -->
                                    <!-- <td></td> -->
                                    <!-- <td>{{$adminblogpost->meta_description}}</td> -->
                                    <!-- <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td> -->

                                    <td>{{Carbon\Carbon::parse($adminblogpost->created_at)->isoFormat('D-MMM-Y')}}</td>
                                    <td>{{Carbon\Carbon::parse($adminblogpost->updated_at)->isoFormat('D-MMM-Y')}}</td>
                                    <td>

                                        <a href="#" class="btn btn-outline-info btn-sm btn-rounded" data-toggle="modal"
                                            data-target="#editcategory{{$adminblogpost->id}}"><i class="fa fa-edit"></i>
                                            Edit</a>


                                        <button class="btn btn-outline-danger btn-sm btn-rounded " id="btn-delete"
                                            data-url="{{route('adminblogpost.destroy',$adminblogpost)}}"><i
                                                class="fas fa-trash"></i></button>
                                    </td>
                                </tr>



                                <!-- edit supplier model -->
                                <div class="modal right fade" id="editcategory{{$adminblogpost->id}}"
                                    data-backdrop="static" tabindex="-1" role="dialog"
                                    aria-labelledby="staticBackdropLabel" aria-hidden="true"
                                    style="overflow:scroll !important;">
                                    <div class="modal-dialog" role="document" style="margin-right: 765px;">
                                        <div class="modal-content" style="width:800px">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="staticBackdropLabel">Edit Blog Post</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$adminblogpost->id}}
                                            </div>
                                            <div class="modal-body">

                                                <form action="{{ route('adminblogpost.update',$adminblogpost) }}"
                                                    method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="form-group col-sm-12">
                                                        <label for="blog_title">Blog Title</label>
                                                        <input type="text" name="blog_title"
                                                            class="form-control @error('blog_title') is-invalid @enderror"
                                                            id="blog_title" placeholder="BLog Title"
                                                            value="{{ old('name',$adminblogpost->blog_title) }}">
                                                        @error('blog_title')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>


                                                    <div class="form-group col-sm-12">
                                                        <label for="blog_description">Blog Description</label>
                                                        <textarea name="blog_description"
                                                            class="form-control @error('blog_description') is-invalid @enderror"
                                                            id="blog_description"
                                                            placeholder="Blog Description">{{ old('blog_description', $adminblogpost->blog_description) }}</textarea>
                                                        @error('blog_description')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-sm-12">
                                                        <label for="blog">Blog</label>
                                                        <textarea name="blog"
                                                            class="form-control @error('blog') is-invalid @enderror"
                                                            id="summernote1"
                                                            rows="20">{!!$adminblogpost->blog!!}</textarea>
                                                        @error('blog')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>


                                                    <div class="form-group col-sm-12">
                                                        <label for="blog_image">Blog Featured Image</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input"
                                                                name="blog_image" id="blog_image">
                                                            <label class="custom-file-label" for="blog_image">Choose
                                                                file</label>
                                                        </div>
                                                        @error('blog_image')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="d-flex form-group">

                                                        <div class="col-sm-6">
                                                            <label for="meta_title">Meta Title</label>
                                                            <input type="text" name="meta_title"
                                                                class="form-control @error('meta_title') is-invalid @enderror"
                                                                id="meta_title" placeholder="Meta Title"
                                                                value="{{ old('meta_title',$adminblogpost->meta_title) }}">
                                                            @error('meta_title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label for="focus_keywords">Focused Keywords</label>
                                                            <input type="text" name="focus_keywords"
                                                                class="form-control @error('focus_keywords') is-invalid @enderror"
                                                                id="focus_keywords" placeholder="Focused Keywords"
                                                                value="{{ old('focus_keywords',$adminblogpost->focus_keywords) }}">
                                                            @error('focus_keywords')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-sm-12">
                                                        <label for="meta_description">Meta Description</label>
                                                        <textarea name="meta_description"
                                                            class="form-control @error('meta_description') is-invalid @enderror"
                                                            id="meta_description"
                                                            placeholder="Blog Description">{{ old('meta_description', $adminblogpost->meta_description) }}</textarea>
                                                        @error('meta_description')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="d-flex form-group">

                                                        <div class="col-sm-6">
                                                            <label for="blog_slug">Blog Slug</label>
                                                            <input type="text" name="blog_slug"
                                                                class="form-control @error('blog_slug') is-invalid @enderror"
                                                                id="blog_slug" placeholder="Blog Slug"
                                                                value="{{ old('blog_slug',$adminblogpost->blog_slug) }}">
                                                            @error('blog_slug')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label for="blog_post_category">Blog Post Category</label>
                                                            <select name="blog_post_category_id" class="form-control"
                                                                required="required">
                                                                @if (isset($admincategoryblogs))
                                                                @foreach ($admincategoryblogs as $cc)
                                                                <option class="blog_post_category_option"
                                                                    value="{{$cc->id}}">
                                                                    {{$cc->name}}</option>
                                                                @endforeach
                                                                @endif

                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="d-flex form-group">

                                                        <div class="col-sm-6">
                                                            <label for="hash_tags">Hash Tags</label>
                                                            <input required type="text" name="hash_tags"
                                                                class="form-control @error('hash_tags') is-invalid @enderror"
                                                                id="BLog Title" placeholder="Hash Tags"
                                                                value="{{ old('hash_tags',$adminblogpost->hash_tags) }}">
                                                            @error('hash_tags')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label for="blog_post_views">Blog Post Views</label>
                                                            <input required type="number" name="blog_post_views"
                                                                class="form-control @error('blog_post_views') is-invalid @enderror"
                                                                id="BLog Title" placeholder="Blog Post Views"
                                                                value="{{ old('blog_post_views',$adminblogpost->blog_post_views) }}">
                                                            @error('blog_post_views')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>




                                                    <button class="btn btn-warning btn-block"
                                                        type="submit">Update</button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                        {{ $adminblogposts->render() }}

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- add supplier model -->

<div class="modal right fade" id="addproduct" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdropLabel" aria-hidden="true" style="overflow:scroll !important;">
    <div class="modal-dialog" role="document" style="margin-right: 750px;">
        <div class="modal-content" style="width:800px">
            <div class="modal-header">
                <h4 class="modal-title" id="staticBackdropLabel">Add Blog Post<h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
            </div>
            <div class="modal-body">

                <form action="{{ route('adminblogpost.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-sm-12">
                        <label for="blog_title">Blog Title</label>
                        <input required type="text" name="blog_title"
                            class="form-control @error('blog_title') is-invalid @enderror" id="BLog Title"
                            placeholder="Blog Title" value="{{ old('blog_title') }}">
                        @error('blog_title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>


                    <div class="form-group col-sm-12">
                        <label for="blog_description">Blog Description</label>
                        <textarea name="blog_description"
                            class="form-control @error('blog_description') is-invalid @enderror" id="description"
                            placeholder="Blog Description">{{ old('blog_description') }}</textarea>
                        @error('blog_description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="blog">Blog</label>
                        <textarea name="blog" class="form-control @error('blog') is-invalid @enderror" id="summernote"
                            rows="20" placeholder="Blog">{{ old('blog') }}</textarea>
                        @error('blog')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>


                    <div class="form-group col-sm-12">
                        <label for="blog_image">Blog Featured Image</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="blog_image" id="blog_image">
                            <label class="custom-file-label" for="blog_image">Choose file</label>
                        </div>
                        @error('blog_image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="d-flex form-group">

                        <div class="col-sm-6">
                            <label for="meta_title">Meta Title</label>
                            <input required type="text" name="meta_title"
                                class="form-control @error('meta_title') is-invalid @enderror" id="BLog Title"
                                placeholder="Meta Title" value="{{ old('meta_title') }}">
                            @error('meta_title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-6">
                            <label for="focus_keywords">Focused Keywords</label>

                            <input required type="text" name="focus_keywords"
                                class="form-control @error('focus_keywords') is-invalid @enderror" id="BLog Title"
                                placeholder="Focused Keywords" value="{{ old('focus_keywords') }}">

                            @error('focus_keywords')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="meta_description">Meta Description</label>
                        <textarea name="meta_description"
                            class="form-control @error('meta_description') is-invalid @enderror" id="description"
                            placeholder="Meta Description">{{ old('meta_description') }}</textarea>
                        @error('meta_description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex form-group">

                        <div class="col-sm-6">
                            <label for="blog_slug">Blog Slug</label>
                            <input required type="text" name="blog_slug"
                                class="form-control @error('blog_slug') is-invalid @enderror" id="BLog Title"
                                placeholder="Blog Slug" value="{{ old('blog_slug') }}">
                            @error('blog_slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-6">
                            <label for="blog_post_category">Blog Post Category</label>
                            <select name="blog_post_category_id" class="form-control" required="required">
                                @if (isset($admincategoryblogs))
                                @foreach ($admincategoryblogs as $cc)
                                <option class="unit_option" value="{{$cc->id}}">{{$cc->name}}</option>
                                @endforeach
                                @endif

                            </select>
                        </div>

                    </div>

                    <div class="d-flex form-group">

                        <div class="col-sm-6">
                            <label for="hash_tags">Hash Tags</label>
                            <input required type="text" name="hash_tags"
                                class="form-control @error('hash_tags') is-invalid @enderror" id="BLog Title"
                                placeholder="Hash Tags" value="{{ old('hash_tags') }}">
                            @error('hash_tags')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-6">
                            <label for="blog_post_views">Blog Post Views</label>
                            <input required type="number" name="blog_post_views"
                                class="form-control @error('blog_post_views') is-invalid @enderror" id="BLog Title"
                                placeholder="Hash Tags" value="{{ old('blog_post_views') }}">
                            @error('blog_post_views')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">Create</button>
                </form>

            </div>
        </div>
    </div>
</div>

<!-- <div id="summernote"></div> -->
<style>
.modal.right .modal-dialog {
    /* position: absolute; */
    top: 0;
    right: 0;
    margin-right: 17.5vh;
}

.modal.fade:not(.in).right .modal-dialog {

    -webkit-transform: translate3d(25%, 0, 0);
    transform: translate3d(25%, 0, 0, 0);
}
</style>
<script>
$('#summernote').summernote({
    placeholder: 'Enter Blog',
    tabsize: 2,
    height: 100
});
</script>
<script>
$('#summernote1').summernote({
    tabsize: 2,
    height: 100
});
</script>
<script src="{{ asset('sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
<script>
$(document).ready(function() {
    $(document).on('click', '#btn-delete', function() {
        $this = $(this);
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "Do you really want to delete this product?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.post($this.data('url'), {
                    _method: 'DELETE',
                    _token: '{{csrf_token()}}'
                }, function(res) {
                    $this.closest('tr').fadeOut(500, function() {
                        $(this).remove();
                    })
                })
            }
        })
    })
});

$(document).ready(function() {
    bsCustomFileInput.init();
});
</script>
@endsection