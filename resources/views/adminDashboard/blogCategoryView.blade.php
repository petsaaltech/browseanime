@extends('adminDashboard.master')

@section('adminMainContent')



<div class="container p-5 mt-4">
        <div class="row mt-5">
            <div class="col-lg-12">
                <h2 class="h1-font-size">Blog Categories List</h2>
                <hr>
            </div>

            <div class="col-md-6 offset-md-2">@if (session('status'))
                <div class="alert alert-success mt-5" style="background-color:#1E1E2D !important; color: whitesmoke;">
                    {{ session('status') }}
                </div>
            @endif
</div>

        </div>
    </div>



    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Category Title</th>
                    <th scope="col">Category Description</th>
                    <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    </tr>
                    
                </tbody>
            </table>
                            </div>
                        </div>
                    </div>
           

@endsection

