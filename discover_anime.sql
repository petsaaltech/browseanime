-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2022 at 01:31 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `discover_anime`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_blog_posts`
--

CREATE TABLE `admin_blog_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `animes`
--

CREATE TABLE `animes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `english_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japanese_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `synopsis` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `popularity` int(11) DEFAULT NULL,
  `youtube_trailer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_episodes` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aired` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `season` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `broadcast` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `animes`
--

INSERT INTO `animes` (`id`, `anime_id`, `english_title`, `japanese_title`, `synopsis`, `rank`, `popularity`, `youtube_trailer`, `type`, `no_of_episodes`, `status`, `aired`, `season`, `broadcast`, `rating`, `duration`, `source`, `anime_image`, `created_at`, `updated_at`) VALUES
(1, 41467, 'Bleach: Thousand-Year Blood War', 'BLEACH 千年血戦篇', 'Was it all just a coincidence, or was it inevitable? Ichigo Kurosaki gained the powers of a Soul Reaper through a chance encounter. As a Substitute Soul Reaper, Ichigo became caught in the turmoil of the Soul Society, a place where deceased souls gather. But with help from his friends, Ichigo overcame every challenge to become even stronger. When new Soul Reapers and a new enemy appear in his hometown of Karakura, Ichigo jumps back into the battlefield with his Zanpakuto to help those in need. Meanwhile, the Soul Society is observing a sudden surge in the number of Hollows being destroyed in the World of the Living. They also receive separate reports of residents in the Rukon District having gone missing. Finally, the Seireitei, home of the Soul Reapers, comes under attack by a group calling themselves the Wandenreich. Led by Yhwach, the father of all Quincies, the Wandenreich declare war against the Soul Reapers with the following message: \"Five days from now, the Soul Society will be annihilated by the Wandenreich.\" The history and truth kept hidden by the Soul Reapers for a thousand long years is finally brought to light. All things must come to an end as Ichigo Kurosaki\'s final battle begins! (Source: official site, translated)', 2, 846, 'https://www.youtube.com/embed/e8YBesRKq_U?enablejsapi=1&wmode=opaque&autoplay=1', 'TV', NULL, 'Currently Airing', 'test', 'fall', 'test', 'R - 17+ (violence & profanity)', '24 min', 'Manga', 'https://cdn.myanimelist.net/images/anime/1764/126627l.jpg', '2022-10-18 03:50:55', '2022-10-18 03:50:55'),
(2, 40748, 'Jujutsu Kaisen', '呪術廻戦', 'Idly indulging in baseless paranormal activities with the Occult Club, high schooler Yuuji Itadori spends his days at either the clubroom or the hospital, where he visits his bedridden grandfather. However, this leisurely lifestyle soon takes a turn for the strange when he unknowingly encounters a cursed item. Triggering a chain of supernatural occurrences, Yuuji finds himself suddenly thrust into the world of Curses—dreadful beings formed from human malice and negativity—after swallowing the said item, revealed to be a finger belonging to the demon Sukuna Ryoumen, the \"King of Curses.\" Yuuji experiences first-hand the threat these Curses pose to society as he discovers his own newfound powers. Introduced to the Tokyo Metropolitan Jujutsu Technical High School, he begins to walk down a path from which he cannot return—the path of a Jujutsu sorcerer. [Written by MAL Rewrite]', 56, 21, 'https://www.youtube.com/embed/4A_X-Dvl0ws?enablejsapi=1&wmode=opaque&autoplay=1', 'TV', 24, 'Finished Airing', 'test', 'fall', 'test', 'R - 17+ (violence & profanity)', '23 min per ep', 'Manga', 'https://cdn.myanimelist.net/images/anime/1171/109222l.jpg', '2022-10-18 03:51:20', '2022-10-18 03:51:20'),
(3, 44387, 'The Night Beyond the Tricornered Window', 'さんかく窓の外側は夜', 'Kousuke Mikado has been able to see spirits for as long as he can remember. Extremely terrified of them, he tries to suppress his ability by wearing glasses. One day, while working at a bookstore, he catches the attention of Rihito Hiyakawa, an eccentric medium who is convinced that they were destined to meet each other. Thanks to Hiyakawa\'s antics, Mikado is thrust into the world of the paranormal, with the former promising that he will shed his fears if Mikado stays with him. As they continue to work together, the two discover a series of malicious curses that all point to the same name—\"Erika Hiura.\" What is this person\'s goal, and how do they connect with Mikado and Hiyakawa? [Written by MAL Rewrite]', 5332, 3122, 'https://www.youtube.com/embed/8QhsBa4bxVU?enablejsapi=1&wmode=opaque&autoplay=1', 'TV', 12, 'Finished Airing', 'test', 'fall', 'test', 'R - 17+ (violence & profanity)', '23 min per ep', 'Manga', 'https://cdn.myanimelist.net/images/anime/1869/118766l.jpg', '2022-10-18 03:52:10', '2022-10-18 03:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `anime_characters`
--

CREATE TABLE `anime_characters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `character_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `character_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `character_role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_characters`
--

INSERT INTO `anime_characters` (`id`, `anime_id`, `character_name`, `character_image`, `character_role`, `created_at`, `updated_at`) VALUES
(1, 41467, 'Ishida, Uryuu', 'https://cdn.myanimelist.net/images/characters/16/139189.jpg?s=c45c92f3d197dbbe891cfd266f487c2f', 'Main', NULL, NULL),
(2, 41467, 'Kurosaki, Ichigo', 'https://cdn.myanimelist.net/images/characters/3/89190.jpg?s=58f441dea1eca62b2e93b3f87a4e666a', 'Main', NULL, NULL),
(3, 41467, 'Abarai, Renji', 'https://cdn.myanimelist.net/images/characters/10/171877.jpg?s=e4fb4773c11884cd26a4eaed44ca3f6d', 'Supporting', NULL, NULL),
(4, 41467, 'Accutrone, Robert', 'https://cdn.myanimelist.net/images/characters/3/272727.jpg?s=a1736d9c2ae5fb8dc2d2d36669429e03', 'Supporting', NULL, NULL),
(5, 41467, 'Aizen, Sousuke', 'https://cdn.myanimelist.net/images/characters/16/73909.jpg?s=1a450d31fd432b46314048dbb3915fc4', 'Supporting', NULL, NULL),
(6, 41467, 'Akon', 'https://cdn.myanimelist.net/images/characters/13/103034.jpg?s=a78454331048761184f678fd8d0bdb35', 'Supporting', NULL, NULL),
(7, 41467, 'Arisawa, Tatsuki', 'https://cdn.myanimelist.net/images/characters/11/102747.jpg?s=f9f1b7153ab66f10d6f25a2685810764', 'Supporting', NULL, NULL),
(8, 41467, 'Ayasegawa, Yumichika', 'https://cdn.myanimelist.net/images/characters/4/33228.jpg?s=91e220ec6468364aa25b9a7ea23bdb8f', 'Supporting', NULL, NULL),
(9, 41467, 'Barro, Lille', 'https://cdn.myanimelist.net/images/characters/3/261763.jpg?s=0182236b2bc327604070693006da06c8', 'Supporting', NULL, NULL),
(10, 41467, 'Basterbine, Bambietta', 'https://cdn.myanimelist.net/images/characters/9/488168.jpg?s=dd3bcf9d4791a3e82a261e84bdd0616d', 'Supporting', NULL, NULL),
(11, 41467, 'Berci, Driscoll', 'https://cdn.myanimelist.net/images/characters/5/175777.jpg?s=e0481b736a364bf7b372e4552a013cc7', 'Supporting', NULL, NULL),
(12, 41467, 'BG9', 'https://cdn.myanimelist.net/images/characters/6/220831.jpg?s=88ca0c3ea01634491dd9c4bf95734870', 'Supporting', NULL, NULL),
(13, 41467, 'Black, Bazzard', 'https://cdn.myanimelist.net/images/characters/6/290662.jpg?s=ca4b1b163b864f994e10f9f4965045d4', 'Supporting', NULL, NULL),
(14, 41467, 'Catnipp, Candice', 'https://cdn.myanimelist.net/images/characters/4/246863.jpg?s=3845a2f1ff9dd855d7ded28807094407', 'Supporting', NULL, NULL),
(15, 41467, 'De Masculine, Mask', 'https://cdn.myanimelist.net/images/characters/12/227245.jpg?s=b335955006254fb1548cd9d71e852b9c', 'Supporting', NULL, NULL),
(16, 41467, 'Dokugamine, Riruka', 'https://cdn.myanimelist.net/images/characters/14/139193.jpg?s=741dfcd48ea0116a2332da8f51a202ff', 'Supporting', NULL, NULL),
(17, 41467, 'Ebern, Asguiaro', 'https://cdn.myanimelist.net/images/characters/10/156019.jpg?s=c01d966aa0d300cfc3e57969afe79cdb', 'Supporting', NULL, NULL),
(18, 41467, 'Friegen, Luders', 'https://cdn.myanimelist.net/images/characters/16/161589.jpg?s=c15a5cd4168ece63f4e016084f1080f5', 'Supporting', NULL, NULL),
(19, 41467, 'Gabrielli, Berenice', 'https://cdn.myanimelist.net/images/characters/2/193249.jpg?s=d02639b809168a6e5ab3bda4daa2cdda', 'Supporting', NULL, NULL),
(20, 41467, 'Guizbatt, Jerome', 'https://cdn.myanimelist.net/images/characters/13/183717.jpg?s=9c7ea8f5c9d007f1b34bc214e28348fd', 'Supporting', NULL, NULL),
(21, 41467, 'Harribel, Tier', 'https://cdn.myanimelist.net/images/characters/7/118659.jpg?s=36d9506c84decc637b5436c564c2098a', 'Supporting', NULL, NULL),
(22, 41467, 'Haschwalth, Jugram', 'https://cdn.myanimelist.net/images/characters/8/290179.jpg?s=641bdcbfe184900f26dfe2f15855814f', 'Supporting', NULL, NULL),
(23, 41467, 'Hikifune, Kirio', 'https://cdn.myanimelist.net/images/characters/8/187399.jpg?s=67ec350d1e15c3455f1eea00909dff09', 'Supporting', NULL, NULL),
(24, 41467, 'Hinamori, Momo', 'https://cdn.myanimelist.net/images/characters/8/33231.jpg?s=03dbd7555461218708c86fd953d633e6', 'Supporting', NULL, NULL),
(25, 41467, 'Hirako, Shinji', 'https://cdn.myanimelist.net/images/characters/3/72979.jpg?s=218ab8454b5512a2d97ea5bcace550ce', 'Supporting', NULL, NULL),
(26, 41467, 'Hisagi, Shuuhei', 'https://cdn.myanimelist.net/images/characters/13/76390.jpg?s=a82209807ae4ef998e7b4399200d8908', 'Supporting', NULL, NULL),
(27, 41467, 'Hitsugaya, Toushirou', 'https://cdn.myanimelist.net/images/characters/11/36579.jpg?s=fec37a5d08dbe1f8e806d3f9ce9d7f4f', 'Supporting', NULL, NULL),
(28, 41467, 'Hyousube, Ichibei', 'https://cdn.myanimelist.net/images/characters/14/187401.jpg?s=f8d50dcd451aec31c1eca79e8687c9db', 'Supporting', NULL, NULL),
(29, 41467, 'Inoue, Orihime', 'https://cdn.myanimelist.net/images/characters/12/97695.jpg?s=fd414e1788ddbd94f043a201c22e7cb6', 'Supporting', NULL, NULL),
(30, 41467, 'Ise, Nanao', 'https://cdn.myanimelist.net/images/characters/2/33233.jpg?s=aaf516f7bb2edc8c9250bd96f329e879', 'Supporting', NULL, NULL),
(31, 41467, 'Ishida, Ryuuken', 'https://cdn.myanimelist.net/images/characters/14/141837.jpg?s=b7019c17b82a7ef5515832609c4a5551', 'Supporting', NULL, NULL),
(32, 41467, 'Jaegerjaquez, Grimmjow', 'https://cdn.myanimelist.net/images/characters/10/159977.jpg?s=1e1b57c13c45a0ba1a1960ce7d2279e6', 'Supporting', NULL, NULL),
(33, 41467, 'Kajoumaru, Hidetomo', 'https://cdn.myanimelist.net/images/characters/12/170965.jpg?s=33d95e518c154d9c47630771f16d6ffc', 'Supporting', NULL, NULL),
(34, 41467, 'Kira, Izuru', 'https://cdn.myanimelist.net/images/characters/11/33234.jpg?s=9d8f4700de0ef47b440523c48cdf8ea9', 'Supporting', NULL, NULL),
(35, 41467, 'Kirinji, Tenjiirou', 'https://cdn.myanimelist.net/images/characters/12/188530.jpg?s=9a803519852edc3f8f56a7b704c23adf', 'Supporting', NULL, NULL),
(36, 41467, 'Komamura, Sajin', 'https://cdn.myanimelist.net/images/characters/16/59028.jpg?s=4cd3017d94232b3480023fb0d230952e', 'Supporting', NULL, NULL),
(37, 41467, 'Kon', 'https://cdn.myanimelist.net/images/characters/8/102740.jpg?s=f91b0f029056ba25d9aa2dbdc088ce92', 'Supporting', NULL, NULL),
(38, 41467, 'Kuchiki, Byakuya', 'https://cdn.myanimelist.net/images/characters/7/100098.jpg?s=c184bfa12ef0b29cc48d31e986d011ad', 'Supporting', NULL, NULL),
(39, 41467, 'Kuchiki, Rukia', 'https://cdn.myanimelist.net/images/characters/2/78215.jpg?s=2a0f7f516f3e5b213833e9f9a9de4a51', 'Supporting', NULL, NULL),
(40, 41467, 'Kurosaki, Isshin', 'https://cdn.myanimelist.net/images/characters/16/97168.jpg?s=894c36b2c23f0b3ce6690d67133f4701', 'Supporting', NULL, NULL),
(41, 41467, 'Kurotsuchi, Mayuri', 'https://cdn.myanimelist.net/images/characters/11/73295.jpg?s=e85fc128d238381c72abffa387bfabef', 'Supporting', NULL, NULL),
(42, 41467, 'Kusajishi, Yachiru', 'https://cdn.myanimelist.net/images/characters/9/85348.jpg?s=e7b37282b1de3e3d6072176c475787af', 'Supporting', NULL, NULL),
(43, 41467, 'Kyouraku, Jirou Souzousuke Shunsui', 'https://cdn.myanimelist.net/images/characters/14/33242.jpg?s=7445a3128d9b5e325c2c49b7a7e6ab92', 'Supporting', NULL, NULL),
(44, 41467, 'Madarame, Ikkaku', 'https://cdn.myanimelist.net/images/characters/14/33243.jpg?s=3fe358bed3801d0465927c401c5004e2', 'Supporting', NULL, NULL),
(45, 41467, 'Madarame, Shino', 'https://cdn.myanimelist.net/images/characters/13/484953.jpg?s=903393640c030a473818e355026fc293', 'Supporting', NULL, NULL),
(46, 41467, 'Matsumoto, Rangiku', 'https://cdn.myanimelist.net/images/characters/8/97855.jpg?s=dd8ae216ebdd70cc8c33f3a1abc80f57', 'Supporting', NULL, NULL),
(47, 41467, 'Muguruma, Kensei', 'https://cdn.myanimelist.net/images/characters/10/45938.jpg?s=8a002399dcc4ce36d37cdf75bccbcfc8', 'Supporting', NULL, NULL),
(48, 41467, 'Najahkoop, NaNaNa', 'https://cdn.myanimelist.net/images/characters/8/234775.jpg?s=bd5fb99654d8122c5cfd3f6a13ee1011', 'Supporting', NULL, NULL),
(49, 41467, 'Nakk Le Vaar, Askin', 'https://cdn.myanimelist.net/images/characters/12/221079.jpg?s=5b883c792051e17c4bbaf1261d9e8f7b', 'Supporting', NULL, NULL),
(50, 41467, 'Nimaiya, Ouetsu', 'https://cdn.myanimelist.net/images/characters/12/189524.jpg?s=b2941178b27dbf4b41f18d948565566f', 'Supporting', NULL, NULL),
(51, 41467, 'Nödt, Äs', 'https://cdn.myanimelist.net/images/characters/7/290663.jpg?s=fbff4ca624535fb4538c6cbe85716cd2', 'Supporting', NULL, NULL),
(52, 41467, 'Opie, Quilge', 'https://cdn.myanimelist.net/images/characters/3/290592.jpg?s=ac2955f2080769fb90b5214c67106b8a', 'Supporting', NULL, NULL),
(53, 41467, 'Otoribashi, Roujuurou', 'https://cdn.myanimelist.net/images/characters/15/40966.jpg?s=c49c176083d920a8f437dba8c6662419', 'Supporting', NULL, NULL),
(54, 41467, 'Sado, Yasutora', 'https://cdn.myanimelist.net/images/characters/2/102739.jpg?s=62a1f21a246934b22b8e46a8db7259bf', 'Supporting', NULL, NULL),
(55, 41467, 'Sarugaki, Hiyori', 'https://cdn.myanimelist.net/images/characters/4/72982.jpg?s=1db28c6a4c237f92b41fb5984f0b0257', 'Supporting', NULL, NULL),
(56, 41467, 'Shihouin, Yoruichi', 'https://cdn.myanimelist.net/images/characters/14/33266.jpg?s=dd4d4e83a5430ce79a26336a474e248e', 'Supporting', NULL, NULL),
(57, 41467, 'Shutara, Senjumaru', 'https://cdn.myanimelist.net/images/characters/7/187403.jpg?s=61f68d4dbb7f7df6896c519d1b351c28', 'Supporting', NULL, NULL),
(58, 41467, 'Soi Fon', 'https://cdn.myanimelist.net/images/characters/14/76136.jpg?s=eca1cd3882ffc6fa556b7001ef0b15b9', 'Supporting', NULL, NULL),
(59, 41467, 'Tadaoki-Sasakibe, Choujirou', 'https://cdn.myanimelist.net/images/characters/14/68554.jpg?s=d6d00e58c75fa55e54d1cd249b04bec1', 'Supporting', NULL, NULL),
(60, 41467, 'Thoumeaux, Gremmy', 'https://cdn.myanimelist.net/images/characters/14/245647.jpg?s=d1c6f57ccf839959b9788ead5318260b', 'Supporting', NULL, NULL),
(61, 41467, 'Tu Oderschvank, Nelliel', 'https://cdn.myanimelist.net/images/characters/14/122507.jpg?s=b3ad735a75d6a48fe24d0da915e4c49e', 'Supporting', NULL, NULL),
(62, 41467, 'Ukitake, Juushirou', 'https://cdn.myanimelist.net/images/characters/2/33251.jpg?s=49a8015bf5e4ef1967ba4df44ad7771e', 'Supporting', NULL, NULL),
(63, 41467, 'Unohana, Retsu', 'https://cdn.myanimelist.net/images/characters/2/33252.jpg?s=913cf51c6745995193ac5befbee94570', 'Supporting', NULL, NULL),
(64, 41467, 'Urahara, Kisuke', 'https://cdn.myanimelist.net/images/characters/15/149491.jpg?s=0f14e8fbbf14dde646817cb8552e8908', 'Supporting', NULL, NULL),
(65, 41467, 'Yamada, Hanatarou', 'https://cdn.myanimelist.net/images/characters/11/68624.jpg?s=17af2c04b899710fe7fc6a6d25932981', 'Supporting', NULL, NULL),
(66, 41467, 'Yamamoto-Genryuusai, Shigekuni', 'https://cdn.myanimelist.net/images/characters/7/33294.jpg?s=91124999bf07aecce422148f97ac6291', 'Supporting', NULL, NULL),
(67, 41467, 'Yhwach', 'https://cdn.myanimelist.net/images/characters/8/396645.jpg?s=cb88dac43b0974aa1af7ed51e731acd9', 'Supporting', NULL, NULL),
(68, 41467, 'Yuki, Ryuunosuke', 'https://cdn.myanimelist.net/images/characters/11/156755.jpg?s=1159441d47e54388be932d8d35ea67e9', 'Supporting', NULL, NULL),
(69, 41467, 'Zangetsu', 'https://cdn.myanimelist.net/images/characters/10/33260.jpg?s=c31ce727ee00010ae64cd28947f4ddc7', 'Supporting', NULL, NULL),
(70, 41467, 'Zaraki, Kenpachi', 'https://cdn.myanimelist.net/images/characters/8/150265.jpg?s=a7bda35cc8518a68480cd334baa5e12a', 'Supporting', NULL, NULL),
(71, 40748, 'Fushiguro, Megumi', 'https://cdn.myanimelist.net/images/characters/2/392689.jpg?s=2395a10b59bb54ec9891c74ef214fea1', 'Main', NULL, NULL),
(72, 40748, 'Gojou, Satoru', 'https://cdn.myanimelist.net/images/characters/15/422168.jpg?s=7c1dfc26a9b3a6652da616a0fec7af01', 'Main', NULL, NULL),
(73, 40748, 'Itadori, Yuuji', 'https://cdn.myanimelist.net/images/characters/11/427601.jpg?s=da25b4ee49906235dded673b9b09e162', 'Main', NULL, NULL),
(74, 40748, 'Kugisaki, Nobara', 'https://cdn.myanimelist.net/images/characters/12/422313.jpg?s=053db48815610ce7fdc280f62a3499ce', 'Main', NULL, NULL),
(75, 40748, 'Chousou', 'https://cdn.myanimelist.net/images/characters/11/436205.jpg?s=35ca307997e8ac040d697b6db024be48', 'Supporting', NULL, NULL),
(76, 40748, 'Dagon', 'https://cdn.myanimelist.net/images/characters/6/414241.jpg?s=e8a33f2aaea72b0f04b98be1deb90011', 'Supporting', NULL, NULL),
(77, 40748, 'Esou', 'https://cdn.myanimelist.net/images/characters/7/436204.jpg?s=04b60749369f8eed82c3642692b6149f', 'Supporting', NULL, NULL),
(78, 40748, 'Fujinuma', 'https://cdn.myanimelist.net/images/characters/14/449238.jpg?s=b4950c4f679826fcf2a1f8ad0c6830b2', 'Supporting', NULL, NULL),
(79, 40748, 'Fujinuma\'s Brother', 'https://cdn.myanimelist.net/images/characters/10/449239.jpg?s=3c34648d5937ace1908e0d7229bb4c65', 'Supporting', NULL, NULL),
(80, 40748, 'Fushiguro, Tsumiki', 'https://cdn.myanimelist.net/images/characters/15/437005.jpg?s=2a852781a432ec72ded62564ca762e9f', 'Supporting', NULL, NULL),
(81, 40748, 'Gakuganji, Yoshinobu', 'https://cdn.myanimelist.net/images/characters/13/432951.jpg?s=6075ec5edcf8f70b87a8e774b89270ed', 'Supporting', NULL, NULL),
(82, 40748, 'Getou, Suguru', 'https://cdn.myanimelist.net/images/characters/11/436339.jpg?s=065bf4e33a5ee03584b24bd366fe9bbf', 'Supporting', NULL, NULL),
(83, 40748, 'Hanami', 'https://cdn.myanimelist.net/images/characters/3/414242.jpg?s=4543a6b62596f1adf3c1f24fd2f96185', 'Supporting', NULL, NULL),
(84, 40748, 'Ieiri, Shouko', 'https://cdn.myanimelist.net/images/characters/13/423951.jpg?s=5287a63c8b798f195d1db973df2351de', 'Supporting', NULL, NULL),
(85, 40748, 'Iguchi', 'https://cdn.myanimelist.net/images/characters/14/419230.jpg?s=90e4a46bf449a2a2115d9d17df306c00', 'Supporting', NULL, NULL),
(86, 40748, 'Ijichi, Kiyotaka', 'https://cdn.myanimelist.net/images/characters/6/435400.jpg?s=7641415da13f5692548ab97229ec46c3', 'Supporting', NULL, NULL),
(87, 40748, 'Ino, Takuma', 'https://cdn.myanimelist.net/images/characters/6/436201.jpg?s=659f941ad1937269825fbfe1a857d94c', 'Supporting', NULL, NULL),
(88, 40748, 'Inumaki, Toge', 'https://cdn.myanimelist.net/images/characters/16/428791.jpg?s=993325edfb779345643f365171b51f32', 'Supporting', NULL, NULL),
(89, 40748, 'Iori, Utahime', 'https://cdn.myanimelist.net/images/characters/3/435404.jpg?s=1627d13c26f9f395bd3c4f286c743bfc', 'Supporting', NULL, NULL),
(90, 40748, 'Itadori, Wasuke', 'https://cdn.myanimelist.net/images/characters/13/470647.jpg?s=319174a02b6088aa43789ea4b6227c0d', 'Supporting', NULL, NULL),
(91, 40748, 'Itou, Shouta', 'https://cdn.myanimelist.net/images/characters/6/449224.jpg?s=4cfb41d1c4582120912bb7ff814eb8de', 'Supporting', NULL, NULL),
(92, 40748, 'Jougo', 'https://cdn.myanimelist.net/images/characters/11/415336.jpg?s=1c6800fbf4d7e4620a8600cd118a0506', 'Supporting', NULL, NULL),
(93, 40748, 'Kamo, Noritoshi', 'https://cdn.myanimelist.net/images/characters/2/430935.jpg?s=43c2932f2095864d481deb8b4f50f551', 'Supporting', NULL, NULL),
(94, 40748, 'Kamo, Noritoshi', 'https://cdn.myanimelist.net/images/characters/3/437914.jpg?s=bb504f83b47c6f4dd1fc9f17c8b35577', 'Supporting', NULL, NULL),
(95, 40748, 'Kechizu', 'https://cdn.myanimelist.net/images/characters/9/460080.jpg?s=3862424ac7e32c3260859d87f50c9e1c', 'Supporting', NULL, NULL),
(96, 40748, 'Kumiya, Juuzou', 'https://cdn.myanimelist.net/images/characters/11/435672.jpg?s=7a53fb2ce6f5e31d3fdd2ff4f0c55050', 'Supporting', NULL, NULL),
(97, 40748, 'Mahito', 'https://cdn.myanimelist.net/images/characters/5/446508.jpg?s=8cfa45381097f1e9a720982061ec087d', 'Supporting', NULL, NULL),
(98, 40748, 'Mei Mei', 'https://cdn.myanimelist.net/images/characters/8/435405.jpg?s=59faccb30565d838c66cc66b88d96272', 'Supporting', NULL, NULL),
(99, 40748, 'Miwa, Kasumi', 'https://cdn.myanimelist.net/images/characters/5/431905.jpg?s=5baf76ca4b20bd7e4997b80689fda24f', 'Supporting', NULL, NULL),
(100, 40748, 'Muta, Koukichi', 'https://cdn.myanimelist.net/images/characters/13/444206.jpg?s=9642d0537cdf16690c55ebbcde0c890b', 'Supporting', NULL, NULL),
(101, 40748, 'Nanami, Kento', 'https://cdn.myanimelist.net/images/characters/3/427139.jpg?s=cb5496c3cc9afa0db9b50766f02224a9', 'Supporting', NULL, NULL),
(102, 40748, 'Nishimiya, Momo', 'https://cdn.myanimelist.net/images/characters/9/435453.jpg?s=0b7635eeca84125d7aacbdcd743b8eb9', 'Supporting', NULL, NULL),
(103, 40748, 'Nitta, Akari', 'https://cdn.myanimelist.net/images/characters/4/436202.jpg?s=9a1aa77f51e4950baeb252fad4f1113b', 'Supporting', NULL, NULL),
(104, 40748, 'Noritoshi\'s Mother', 'https://cdn.myanimelist.net/images/characters/14/449237.jpg?s=787d677089ec0885567b3866af1f1bcf', 'Supporting', NULL, NULL),
(105, 40748, 'Okazaki, Tadashi', 'https://cdn.myanimelist.net/images/characters/12/449230.jpg?s=ba3fa7351bfdb4f5b9eeac9a8376dbb5', 'Supporting', NULL, NULL),
(106, 40748, 'Okazaki, Chisaki', 'https://cdn.myanimelist.net/images/characters/9/449233.jpg?s=e5a6aa39713e6c79e89b0a1b95230811', 'Supporting', NULL, NULL),
(107, 40748, 'Panda', 'https://cdn.myanimelist.net/images/characters/16/423950.jpg?s=d898dc5a3174aee1cdcd4108cbdb36c6', 'Supporting', NULL, NULL),
(108, 40748, 'Ryoumen, Sukuna', 'https://cdn.myanimelist.net/images/characters/6/431152.jpg?s=776045fc0fe9f2ecf8923f0f9c20f288', 'Supporting', NULL, NULL),
(109, 40748, 'Saitama School Delinquent', 'https://cdn.myanimelist.net/images/characters/10/449240.jpg?s=22ffd5ad66321ff8dd807625d1f8e8ba', 'Supporting', NULL, NULL),
(110, 40748, 'Saori', 'https://cdn.myanimelist.net/images/characters/2/449236.jpg?s=a0583653e6dcc8948034ea81a5416eb1', 'Supporting', NULL, NULL),
(111, 40748, 'Sasaki', 'https://cdn.myanimelist.net/images/characters/3/419229.jpg?s=f0796fea6b95836ed7a55f52f3aff16c', 'Supporting', NULL, NULL),
(112, 40748, 'Shigemo, Haruta', 'https://cdn.myanimelist.net/images/characters/15/435673.jpg?s=b3f4fcd0b4d7273b7e2105b0ad02cee5', 'Supporting', NULL, NULL),
(113, 40748, 'Sotomura', 'https://cdn.myanimelist.net/images/characters/4/449227.jpg?s=5a87d82d223adb543c2e11543d39ef68', 'Supporting', NULL, NULL),
(114, 40748, 'Student Council President', 'https://cdn.myanimelist.net/images/characters/4/449234.jpg?s=b14065f221f7277de822e295ec71fbb2', 'Supporting', NULL, NULL),
(115, 40748, 'Takada, Nobuko', 'https://cdn.myanimelist.net/images/characters/14/470648.jpg?s=acb26a19d8ad3c3ce3703c69ebda8eea', 'Supporting', NULL, NULL),
(116, 40748, 'Takagi', 'https://cdn.myanimelist.net/images/characters/7/419231.jpg?s=74f884a569606287a4378e4e7018ed8f', 'Supporting', NULL, NULL),
(117, 40748, 'Takashi', 'https://cdn.myanimelist.net/images/characters/2/460085.jpg?s=c0f08e0ed86c9b9f2360ae6d61cf94e8', 'Supporting', NULL, NULL),
(118, 40748, 'Takeda', 'https://cdn.myanimelist.net/images/characters/15/449241.jpg?s=c02889239ade2446077d460dd0e511ef', 'Supporting', NULL, NULL),
(119, 40748, 'Toudou, Aoi', 'https://cdn.myanimelist.net/images/characters/5/427604.jpg?s=4471dfaee135cd387a053f4b6556b2b2', 'Supporting', NULL, NULL),
(120, 40748, 'Tsubasa', 'https://cdn.myanimelist.net/images/characters/7/449225.jpg?s=fa3470163eb53996b4fead7cb96aa064', 'Supporting', NULL, NULL),
(121, 40748, 'Tsukumo, Yuki', 'https://cdn.myanimelist.net/images/characters/3/435406.jpg?s=cb68e33c219068625b4a23357ab2374b', 'Supporting', NULL, NULL),
(122, 40748, 'Uraume', 'https://cdn.myanimelist.net/images/characters/2/436733.jpg?s=c8089d5c1e35e2368a53f771ad49cf8b', 'Supporting', NULL, NULL),
(123, 40748, 'Yaga, Masamichi', 'https://cdn.myanimelist.net/images/characters/7/431153.jpg?s=dc3960cff5fcde4f9feb70eeb49ff47f', 'Supporting', NULL, NULL),
(124, 40748, 'Yoshino, Junpei', 'https://cdn.myanimelist.net/images/characters/13/434571.jpg?s=8c6f56ed8d59120e9e0d8b85c9997057', 'Supporting', NULL, NULL),
(125, 40748, 'Yoshino, Nagi', 'https://cdn.myanimelist.net/images/characters/16/435742.jpg?s=296c1f925d0971b21c1ea9665836a48a', 'Supporting', NULL, NULL),
(126, 40748, 'Zenin, Naobito', 'https://cdn.myanimelist.net/images/characters/2/414320.jpg?s=cef151ad599bb1800099a5e21e4bc42d', 'Supporting', NULL, NULL),
(127, 40748, 'Zenin, Maki', 'https://cdn.myanimelist.net/images/characters/3/440469.jpg?s=5cc1cf7ab69119f871027b35bce53a50', 'Supporting', NULL, NULL),
(128, 40748, 'Zenin, Mai', 'https://cdn.myanimelist.net/images/characters/6/430336.jpg?s=b958cdf63203469623dcaccb38986280', 'Supporting', NULL, NULL),
(129, 44387, 'Hiyakawa, Rihito', 'https://cdn.myanimelist.net/images/characters/15/449999.jpg?s=ee16b8e429f09db31023eda1f643c7a7', 'Main', NULL, NULL),
(130, 44387, 'Mikado, Kousuke', 'https://cdn.myanimelist.net/images/characters/3/450000.jpg?s=5a7e5a69c2d7e94265785b69ba08fddd', 'Main', NULL, NULL),
(131, 44387, 'Hanzawa, Hiroki', 'https://cdn.myanimelist.net/images/characters/10/480454.jpg?s=8592185bc01e93f7258ac8f1b841a5e5', 'Supporting', NULL, NULL),
(132, 44387, 'Hiura, Erika', 'https://cdn.myanimelist.net/images/characters/12/449998.jpg?s=29417481f07b8946685607db399862b9', 'Supporting', NULL, NULL),
(133, 44387, 'Hiura\'s Father', 'https://cdn.myanimelist.net/images/questionmark_23.gif?s=f7dcbc4a4603d18356d3dfef8abd655c', 'Supporting', NULL, NULL),
(134, 44387, 'Isogaki, Shigeo', 'https://cdn.myanimelist.net/images/questionmark_23.gif?s=f7dcbc4a4603d18356d3dfef8abd655c', 'Supporting', NULL, NULL),
(135, 44387, 'Kana', 'https://cdn.myanimelist.net/images/characters/13/456196.jpg?s=18f2b1be1726b5747531fc9257552420', 'Supporting', NULL, NULL),
(136, 44387, 'Kinai, Saori', 'https://cdn.myanimelist.net/images/characters/5/456669.jpg?s=b17e5071a50da36f07fbb7b3dc2f68ed', 'Supporting', NULL, NULL),
(137, 44387, 'Mikado\'s Mother', 'https://cdn.myanimelist.net/images/characters/5/456802.jpg?s=6178c7615c15fc6f10d20beb256432f7', 'Supporting', NULL, NULL),
(138, 44387, 'Miyuki', 'https://cdn.myanimelist.net/images/characters/5/456668.jpg?s=8a42bb137ecaa7fe298d051a8bdb3df6', 'Supporting', NULL, NULL),
(139, 44387, 'Mukae, Keita', 'https://cdn.myanimelist.net/images/characters/11/449997.jpg?s=3ac8447ec0363f3f8c33e18ae0f02000', 'Supporting', NULL, NULL),
(140, 44387, 'Sakaki, Kazuomi', 'https://cdn.myanimelist.net/images/characters/15/450002.jpg?s=846b56dbd21be9d21374ec7799ab6df3', 'Supporting', NULL, NULL),
(141, 44387, 'Sensei', 'https://cdn.myanimelist.net/images/characters/3/456667.jpg?s=4161ec15e41b299940f8024ff42649d1', 'Supporting', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_details`
--

CREATE TABLE `anime_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `genres` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_details`
--

INSERT INTO `anime_details` (`id`, `anime_id`, `genres`, `created_at`, `updated_at`) VALUES
(1, 41467, 'Action', NULL, NULL),
(2, 41467, 'Adventure', NULL, NULL),
(3, 41467, 'Fantasy', NULL, NULL),
(4, 40748, 'Action', NULL, NULL),
(5, 40748, 'Award Winning', NULL, NULL),
(6, 40748, 'Fantasy', NULL, NULL),
(7, 44387, 'Boys Love', NULL, NULL),
(8, 44387, 'Drama', NULL, NULL),
(9, 44387, 'Horror', NULL, NULL),
(10, 44387, 'Mystery', NULL, NULL),
(11, 44387, 'Supernatural', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_episodes`
--

CREATE TABLE `anime_episodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `anime_picutres`
--

CREATE TABLE `anime_picutres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `anime_picture_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_picutres`
--

INSERT INTO `anime_picutres` (`id`, `anime_id`, `anime_picture_url`, `created_at`, `updated_at`) VALUES
(1, 41467, 'https://cdn.myanimelist.net/images/anime/1785/106505l.jpg', NULL, NULL),
(2, 41467, 'https://cdn.myanimelist.net/images/anime/1596/106575l.jpg', NULL, NULL),
(3, 41467, 'https://cdn.myanimelist.net/images/anime/1256/120125l.jpg', NULL, NULL),
(4, 41467, 'https://cdn.myanimelist.net/images/anime/1412/121030l.jpg', NULL, NULL),
(5, 41467, 'https://cdn.myanimelist.net/images/anime/1731/124971l.jpg', NULL, NULL),
(6, 41467, 'https://cdn.myanimelist.net/images/anime/1959/124973l.jpg', NULL, NULL),
(7, 41467, 'https://cdn.myanimelist.net/images/anime/1956/126621l.jpg', NULL, NULL),
(8, 41467, 'https://cdn.myanimelist.net/images/anime/1764/126627l.jpg', NULL, NULL),
(9, 40748, 'https://cdn.myanimelist.net/images/anime/1909/104931l.jpg', NULL, NULL),
(10, 40748, 'https://cdn.myanimelist.net/images/anime/1046/107701l.jpg', NULL, NULL),
(11, 40748, 'https://cdn.myanimelist.net/images/anime/1171/109222l.jpg', NULL, NULL),
(12, 40748, 'https://cdn.myanimelist.net/images/anime/1937/111424l.jpg', NULL, NULL),
(13, 40748, 'https://cdn.myanimelist.net/images/anime/1132/128063l.jpg', NULL, NULL),
(14, 40748, 'https://cdn.myanimelist.net/images/anime/1610/128064l.jpg', NULL, NULL),
(15, 40748, 'https://cdn.myanimelist.net/images/anime/1844/128065l.jpg', NULL, NULL),
(16, 40748, 'https://cdn.myanimelist.net/images/anime/1568/128066l.jpg', NULL, NULL),
(17, 44387, 'https://cdn.myanimelist.net/images/anime/1400/110617l.jpg', NULL, NULL),
(18, 44387, 'https://cdn.myanimelist.net/images/anime/1496/116220l.jpg', NULL, NULL),
(19, 44387, 'https://cdn.myanimelist.net/images/anime/1712/117547l.jpg', NULL, NULL),
(20, 44387, 'https://cdn.myanimelist.net/images/anime/1869/118766l.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_producers`
--

CREATE TABLE `anime_producers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `producers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_producers`
--

INSERT INTO `anime_producers` (`id`, `anime_id`, `producers`, `created_at`, `updated_at`) VALUES
(1, 41467, 'TV Tokyo', NULL, NULL),
(2, 41467, 'Aniplex', NULL, NULL),
(3, 41467, 'Dentsu', NULL, NULL),
(4, 41467, 'Shueisha', NULL, NULL),
(5, 40748, 'Mainichi Broadcasting System', NULL, NULL),
(6, 40748, 'TOHO animation', NULL, NULL),
(7, 40748, 'Shueisha', NULL, NULL),
(8, 40748, 'dugout', NULL, NULL),
(9, 40748, 'Sumzap', NULL, NULL),
(10, 44387, 'MediaNet', NULL, NULL),
(11, 44387, 'Avex Pictures', NULL, NULL),
(12, 44387, 'bilibili', NULL, NULL),
(13, 44387, 'Crunchyroll', NULL, NULL),
(14, 44387, 'Yomiuri TV Enterprise', NULL, NULL),
(15, 44387, 'A-Sketch', NULL, NULL),
(16, 44387, 'Libre', NULL, NULL),
(17, 44387, 'animate', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_recommendations`
--

CREATE TABLE `anime_recommendations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `anime_mal_id` int(11) DEFAULT NULL,
  `anime_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_recommendations`
--

INSERT INTO `anime_recommendations` (`id`, `anime_id`, `anime_mal_id`, `anime_picture`, `anime_title`, `created_at`, `updated_at`) VALUES
(1, 41467, 40748, 'https://cdn.myanimelist.net/images/anime/1171/109222l.jpg?s=f5508bab9e7d610a28f12d1828a6ee79', 'Jujutsu Kaisen', NULL, NULL),
(2, 40748, 38000, 'https://cdn.myanimelist.net/images/anime/1286/99889l.jpg?s=e497d08bef31ae412e314b90a54acfda', 'Kimetsu no Yaiba', NULL, NULL),
(3, 40748, 269, 'https://cdn.myanimelist.net/images/anime/3/40451l.jpg?s=3aa217eced217b3b4223af21c30fe2ed', 'Bleach', NULL, NULL),
(4, 40748, 20, 'https://cdn.myanimelist.net/images/anime/13/17405l.jpg?s=59241469eb470604a792add6fbe7cce6', 'Naruto', NULL, NULL),
(5, 40748, 20507, 'https://cdn.myanimelist.net/images/anime/1886/128266l.jpg?s=a74efc6fc8fa714f6edb13c9f6222910', 'Noragami', NULL, NULL),
(6, 40748, 11061, 'https://cdn.myanimelist.net/images/anime/1337/99013l.jpg?s=1838e905a0aa3542a009fbcf78000701', 'Hunter x Hunter (2011)', NULL, NULL),
(7, 40748, 9919, 'https://cdn.myanimelist.net/images/anime/10/75195l.jpg?s=c2bb5ebd6f79c4ae21272da8215d189f', 'Ao no Exorcist', NULL, NULL),
(8, 40748, 1735, 'https://cdn.myanimelist.net/images/anime/1565/111305l.jpg?s=a92272fe7a37f1c114011b406d5390c8', 'Naruto: Shippuuden', NULL, NULL),
(9, 40748, 31964, 'https://cdn.myanimelist.net/images/anime/10/78745l.jpg?s=8ea4cb2e8a861e63757d3c05aa5d32c2', 'Boku no Hero Academia', NULL, NULL),
(10, 40748, 22535, 'https://cdn.myanimelist.net/images/anime/3/73178l.jpg?s=aeb28b4778c7bcdfcb0004cbb1a55523', 'Kiseijuu: Sei no Kakuritsu', NULL, NULL),
(11, 40748, 41353, 'https://cdn.myanimelist.net/images/anime/1722/107269l.jpg?s=06aa2328ff29a9401217f3b9383ccbc9', 'The God of High School', NULL, NULL),
(12, 40748, 32182, 'https://cdn.myanimelist.net/images/anime/8/80356l.jpg?s=1d7f8902c64d166b821e50ea68267c2a', 'Mob Psycho 100', NULL, NULL),
(13, 40748, 40908, 'https://cdn.myanimelist.net/images/anime/1258/108331l.jpg?s=6293529a9cee6f5c6e3e628cb85f8953', 'Kemono Jihen', NULL, NULL),
(14, 40748, 31478, 'https://cdn.myanimelist.net/images/anime/3/79409l.jpg?s=2885c50dd479ff9260ca95f5e1b786d6', 'Bungou Stray Dogs', NULL, NULL),
(15, 40748, 18153, 'https://cdn.myanimelist.net/images/anime/3/85468l.jpg?s=4f7cc2a1b3cae7557e6ba71097a0a2ae', 'Kyoukai no Kanata', NULL, NULL),
(16, 40748, 22319, 'https://cdn.myanimelist.net/images/anime/5/64449l.jpg?s=f1af76501ac3d4238170191d5e0679f2', 'Tokyo Ghoul', NULL, NULL),
(17, 40748, 44511, 'https://cdn.myanimelist.net/images/anime/1806/126216l.jpg?s=8e4d9184579f166e9adff4987b274192', 'Chainsaw Man', NULL, NULL),
(18, 40748, 392, 'https://cdn.myanimelist.net/images/anime/1228/111372l.jpg?s=564b60dca1108a5de9706ee146556481', 'Yuu☆Yuu☆Hakusho', NULL, NULL),
(19, 40748, 30276, 'https://cdn.myanimelist.net/images/anime/12/76049l.jpg?s=40b6c7dbbbb94c44675116d301150078', 'One Punch Man', NULL, NULL),
(20, 40748, 48483, 'https://cdn.myanimelist.net/images/anime/1277/117155l.jpg?s=20e663be025d176848f4c98eb42f0ab4', 'Mieruko-chan', NULL, NULL),
(21, 40748, 39017, 'https://cdn.myanimelist.net/images/anime/1310/117188l.jpg?s=b94ab025f6901c823082b44a0c5ed28d', 'Kyokou Suiri', NULL, NULL),
(22, 40748, 35120, 'https://cdn.myanimelist.net/images/anime/2/89973l.jpg?s=717c05245eb3166f30bac5945b9cc6c7', 'Devilman: Crybaby', NULL, NULL),
(23, 40748, 1482, 'https://cdn.myanimelist.net/images/anime/13/75194l.jpg?s=8fdc287c0abdce3b85f2b4aa0da94525', 'D.Gray-man', NULL, NULL),
(24, 40748, 38671, 'https://cdn.myanimelist.net/images/anime/1664/103275l.jpg?s=862177f32b646985d9ad953cdee042f6', 'Enen no Shouboutai', NULL, NULL),
(25, 40748, 34572, 'https://cdn.myanimelist.net/images/anime/2/88336l.jpg?s=91adfb36d2c14c84ca2bd10489cdaad8', 'Black Clover', NULL, NULL),
(26, 40748, 3588, 'https://cdn.myanimelist.net/images/anime/9/7804l.jpg?s=79bc552f49367091976b90b724c4eb0d', 'Soul Eater', NULL, NULL),
(27, 40748, 5114, 'https://cdn.myanimelist.net/images/anime/1223/96541l.jpg?s=faffcb677a5eacd17bf761edd78bfb3f', 'Fullmetal Alchemist: Brotherhood', NULL, NULL),
(28, 40748, 14719, 'https://cdn.myanimelist.net/images/anime/3/40409l.jpg?s=eade1f76434383f5af5243cc52d50316', 'JoJo no Kimyou na Bouken (TV)', NULL, NULL),
(29, 40748, 9513, 'https://cdn.myanimelist.net/images/anime/3/28013l.jpg?s=33c1e4b2fc322189b6799b171e342a04', 'Beelzebub', NULL, NULL),
(30, 40748, 26243, 'https://cdn.myanimelist.net/images/anime/5/73474l.jpg?s=8efa86bb99fc22162fa48a46bf82f810', 'Owari no Seraph', NULL, NULL),
(31, 40748, 47778, 'https://cdn.myanimelist.net/images/anime/1908/120036l.jpg?s=34065463bac342e6efe8bfdbed5d24de', 'Kimetsu no Yaiba: Yuukaku-hen', NULL, NULL),
(32, 40748, 37991, 'https://cdn.myanimelist.net/images/anime/1572/95010l.jpg?s=88db8e76960fce4e7aaa69f71cfbb4cb', 'JoJo no Kimyou na Bouken Part 5: Ougon no Kaze', NULL, NULL),
(33, 40748, 38408, 'https://cdn.myanimelist.net/images/anime/1412/107914l.jpg?s=26b333f52b5f693de7da30f666e6f622', 'Boku no Hero Academia 4th Season', NULL, NULL),
(34, 40748, 17505, 'https://cdn.myanimelist.net/images/anime/12/48827l.jpg?s=de9e1184509e21ae4c769f1eca00e0c8', 'Mushibugyou', NULL, NULL),
(35, 40748, 41345, 'https://cdn.myanimelist.net/images/anime/1903/111646l.jpg?s=1bf542fe221313dd7eed0e45d3330059', 'Noblesse', NULL, NULL),
(36, 40748, 48556, 'https://cdn.myanimelist.net/images/anime/1449/117797l.jpg?s=64c5ee29554d4c5716bdac853bb217f1', 'Takt Op. Destiny', NULL, NULL),
(37, 40748, 42625, 'https://cdn.myanimelist.net/images/anime/1293/115173l.jpg?s=ff6994663d2cdb0b96108934793c6d14', 'Heion Sedai no Idaten-tachi', NULL, NULL),
(38, 40748, 41467, 'https://cdn.myanimelist.net/images/anime/1764/126627l.jpg?s=196ea098ac1becd97fc0a4b50a949c2b', 'Bleach: Sennen Kessen-hen', NULL, NULL),
(39, 40748, 38691, 'https://cdn.myanimelist.net/images/anime/1613/102576l.jpg?s=44aa2af9cfc2523f9feb07d2b13d4196', 'Dr. Stone', NULL, NULL),
(40, 40748, 37510, 'https://cdn.myanimelist.net/images/anime/1918/96303l.jpg?s=b5b51cff7ba201e4f1acf37f4f44e224', 'Mob Psycho 100 II', NULL, NULL),
(41, 40748, 18497, 'https://cdn.myanimelist.net/images/anime/5/52563l.jpg?s=4416e561db80ad98145b5ea467738155', 'Yozakura Quartet: Hana no Uta', NULL, NULL),
(42, 40748, 41587, 'https://cdn.myanimelist.net/images/anime/1911/113611l.jpg?s=b397359f9a2b77d7943ae13115d1257d', 'Boku no Hero Academia 5th Season', NULL, NULL),
(43, 40748, 44387, 'https://cdn.myanimelist.net/images/anime/1869/118766l.jpg?s=46f0d6db289d9af19ca4da1271947d54', 'Sankaku Mado no Sotogawa wa Yoru', NULL, NULL),
(44, 40748, 20583, 'https://cdn.myanimelist.net/images/anime/7/76014l.jpg?s=ef5c00cb929dcd690c87f56e6d1b0c8a', 'Haikyuu!!', NULL, NULL),
(45, 40748, 13601, 'https://cdn.myanimelist.net/images/anime/5/43399l.jpg?s=a5f419aedb5908dfd0a419e6abfb588b', 'Psycho-Pass', NULL, NULL),
(46, 40748, 16498, 'https://cdn.myanimelist.net/images/anime/10/47347l.jpg?s=29949c6e892df123f0b0563e836d3d98', 'Shingeki no Kyojin', NULL, NULL),
(47, 40748, 32105, 'https://cdn.myanimelist.net/images/anime/12/79556l.jpg?s=df9594d7a0ea4f9173fee5094a378c5a', 'Sousei no Onmyouji', NULL, NULL),
(48, 40748, 2593, 'https://cdn.myanimelist.net/images/anime/12/21741l.jpg?s=be172e5381adaf4dee00539b323bfd2c', 'Kara no Kyoukai Movie 1: Fukan Fuukei', NULL, NULL),
(49, 40748, 9756, 'https://cdn.myanimelist.net/images/anime/11/55225l.jpg?s=cff930c5de079dbeab2107067050e03c', 'Mahou Shoujo Madoka★Magica', NULL, NULL),
(50, 40748, 38668, 'https://cdn.myanimelist.net/images/anime/1740/104786l.jpg?s=09d26f1f93ec4a1f73e2f0734d63f442', 'Dorohedoro', NULL, NULL),
(51, 40748, 29854, 'https://cdn.myanimelist.net/images/anime/8/74945l.jpg?s=ff85bb8e8f443343880567dc9a1731f7', 'Ushio to Tora (TV)', NULL, NULL),
(52, 40748, 33506, 'https://cdn.myanimelist.net/images/anime/5/85201l.jpg?s=98f1a88ffabf7c0295847361307d0857', 'Ao no Exorcist: Kyoto Fujouou-hen', NULL, NULL),
(53, 40748, 174, 'https://cdn.myanimelist.net/images/anime/10/8380l.jpg?s=22c21a67aaed7da8b28c540d141a6b60', 'Tenjou Tenge', NULL, NULL),
(54, 40748, 11633, 'https://cdn.myanimelist.net/images/anime/11/47677l.jpg?s=6305d2a6b71738d6d7d79e59ca5418c6', 'Blood Lad', NULL, NULL),
(55, 40748, 1535, 'https://cdn.myanimelist.net/images/anime/9/9453l.jpg?s=b89e80691ac5cc0610847ccbe0b8424a', 'Death Note', NULL, NULL),
(56, 40748, 481, 'https://cdn.myanimelist.net/images/anime/3/77055l.jpg?s=9f334a5d9a2c45db980f7a5ff2c3a6f4', 'Yu☆Gi☆Oh! Duel Monsters', NULL, NULL),
(57, 40748, 482, 'https://cdn.myanimelist.net/images/anime/6/20873l.jpg?s=e4faf179b83f881a60b10fc136892d37', 'Yu☆Gi☆Oh!: Duel Monsters GX', NULL, NULL),
(58, 40748, 857, 'https://cdn.myanimelist.net/images/anime/11/18227l.jpg?s=02721d59b37420549d6ab16b82cabb09', 'Air Gear', NULL, NULL),
(59, 40748, 34451, 'https://cdn.myanimelist.net/images/anime/3/88282l.jpg?s=8e72cada2526b48e61996dce69b964af', 'Kekkai Sensen & Beyond', NULL, NULL),
(60, 40748, 42307, 'https://cdn.myanimelist.net/images/anime/1016/111566l.jpg?s=038dddfb11cd94b59c5b49eae11adf21', 'Subarashiki Kono Sekai The Animation', NULL, NULL),
(61, 40748, 16011, 'https://cdn.myanimelist.net/images/anime/13/75094l.jpg?s=df68546dafe5c297183d9c7b52d104f8', 'Tokyo Ravens', NULL, NULL),
(62, 40748, 164, 'https://cdn.myanimelist.net/images/anime/7/75919l.jpg?s=0d877ed9aa60cc49fdb128a2f68386e2', 'Mononoke Hime', NULL, NULL),
(63, 40748, 1017, 'https://cdn.myanimelist.net/images/anime/13/61839l.jpg?s=4868e988456e5e635b9bdab5633dd863', 'Majutsushi Orphen', NULL, NULL),
(64, 40748, 37520, 'https://cdn.myanimelist.net/images/anime/1879/100467l.jpg?s=fd59137a5eb37dbabe9510bde27fe4ba', 'Dororo', NULL, NULL),
(65, 40748, 11617, 'https://cdn.myanimelist.net/images/anime/1331/111940l.jpg?s=ff42c65c550aa02f8f55ffb22c6a00ce', 'High School DxD', NULL, NULL),
(66, 40748, 29758, 'https://cdn.myanimelist.net/images/anime/12/80197l.jpg?s=fad7ec2f9ac46d4b96ff1bb2a923c60e', 'Taboo Tattoo', NULL, NULL),
(67, 40748, 1860, 'https://cdn.myanimelist.net/images/anime/1894/113730l.jpg?s=a770abe42940ca0a28db931f6e53461c', 'Tokyo Majin Gakuen Kenpucho: Tou', NULL, NULL),
(68, 40748, 22297, 'https://cdn.myanimelist.net/images/anime/12/67333l.jpg?s=bc2c38201e676d7c50e27246c740aa5c', 'Fate/stay night: Unlimited Blade Works', NULL, NULL),
(69, 40748, 28171, 'https://cdn.myanimelist.net/images/anime/3/72943l.jpg?s=a93f0e87eb1a78ff9dd281cfbb236bbf', 'Shokugeki no Souma', NULL, NULL),
(70, 40748, 39534, 'https://cdn.myanimelist.net/images/anime/1050/111687l.jpg?s=7b7b9b84ca3645e8a525379422fd39c9', 'Jibaku Shounen Hanako-kun', NULL, NULL),
(71, 44387, 37522, 'https://cdn.myanimelist.net/images/anime/1399/94794l.jpg?s=8480949359b42b78bb71f8904f870a17', 'Pet', NULL, NULL),
(72, 44387, 40748, 'https://cdn.myanimelist.net/images/anime/1171/109222l.jpg?s=f5508bab9e7d610a28f12d1828a6ee79', 'Jujutsu Kaisen', NULL, NULL),
(73, 44387, 39761, 'https://cdn.myanimelist.net/images/anime/1070/110685l.jpg?s=63b540e0eac80b4a51f3f2e825f88975', 'Saezuru Tori wa Habatakanai: The Clouds Gather', NULL, NULL),
(74, 44387, 44064, 'https://cdn.myanimelist.net/images/anime/1042/119432l.jpg?s=f9414a3eddbbc0ad3a136ea44c39bb1a', 'Lie Huo Jiao Chou', NULL, NULL),
(75, 44387, 40230, 'https://cdn.myanimelist.net/images/anime/1134/109558l.jpg?s=ee2c4f8230fbf83f8d629c8eec6cae4a', 'Housekishou Richard-shi no Nazo Kantei', NULL, NULL),
(76, 44387, 1571, 'https://cdn.myanimelist.net/images/anime/11/73908l.jpg?s=1c404e7eb8d8954126c8a37e9b6e50b0', 'Ghost Hunt', NULL, NULL),
(77, 44387, 2593, 'https://cdn.myanimelist.net/images/anime/12/21741l.jpg?s=be172e5381adaf4dee00539b323bfd2c', 'Kara no Kyoukai Movie 1: Fukan Fuukei', NULL, NULL),
(78, 44387, 33605, 'https://cdn.myanimelist.net/images/anime/9/81045l.jpg?s=3ccca76680cd5dfbca58c45179ebe9d0', 'Ling Qi', NULL, NULL),
(79, 44387, 32182, 'https://cdn.myanimelist.net/images/anime/8/80356l.jpg?s=1d7f8902c64d166b821e50ea68267c2a', 'Mob Psycho 100', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_reviews`
--

CREATE TABLE `anime_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `story` int(11) DEFAULT NULL,
  `animation` int(11) DEFAULT NULL,
  `characters` int(11) DEFAULT NULL,
  `music` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `anime_seaches`
--

CREATE TABLE `anime_seaches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `anime_streamings`
--

CREATE TABLE `anime_streamings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `streaming_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `streaming_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_streamings`
--

INSERT INTO `anime_streamings` (`id`, `anime_id`, `streaming_title`, `streaming_url`, `created_at`, `updated_at`) VALUES
(1, 40748, 'Crunchyroll', 'http://www.crunchyroll.com/series-279979', NULL, NULL),
(2, 40748, 'Funimation', 'https://www.funimation.com/shows/jujutsu-kaisen/', NULL, NULL),
(3, 40748, 'Animax Mongolia', 'https://www.animax.app/', NULL, NULL),
(4, 44387, 'Crunchyroll', 'http://www.crunchyroll.com/series-281912', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anime_views`
--

CREATE TABLE `anime_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anime_views`
--

INSERT INTO `anime_views` (`id`, `anime_id`, `views`, `created_at`, `updated_at`) VALUES
(1, 41467, 1, '2022-10-18 03:50:54', '2022-10-18 03:50:54'),
(2, 40748, 1, '2022-10-18 03:51:19', '2022-10-18 03:51:19'),
(3, 44387, 1, '2022-10-18 03:52:09', '2022-10-18 03:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `countryName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `horror_animes`
--

CREATE TABLE `horror_animes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `anime_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `popularity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_13_201245_create_anime_views_table', 1),
(6, '2022_09_17_080751_create_anime_seaches_table', 1),
(7, '2022_09_17_094733_create_animes_table', 1),
(8, '2022_09_17_122450_create_anime_details_table', 1),
(9, '2022_09_17_141328_create_anime_producers_table', 1),
(10, '2022_09_17_160536_create_anime_episodes_table', 1),
(11, '2022_09_17_160934_create_anime_characters_table', 1),
(12, '2022_09_18_055656_create_anime_picutres_table', 1),
(13, '2022_09_18_062121_create_anime_recommendations_table', 1),
(14, '2022_09_18_105246_create_popular_animes_table', 1),
(15, '2022_09_18_125746_create_horror_animes_table', 1),
(16, '2022_09_19_121637_create_anime_streamings_table', 1),
(17, '2022_09_19_203553_create_ranked_animes_table', 1),
(18, '2022_09_21_210855_create_user_favourite_lists_table', 1),
(19, '2022_09_23_090652_create_user_details_table', 1),
(20, '2022_09_25_190332_create_user_anime_status_lists_table', 1),
(21, '2022_09_27_130542_create_anime_reviews_table', 1),
(22, '2022_10_11_081339_create_admins_table', 1),
(23, '2022_10_11_134942_create_blog_posts_table', 1),
(24, '2022_10_11_135031_create_admin_blog_posts_table', 1),
(25, '2022_10_16_114614_create_top_airing_animes_table', 1),
(26, '2022_10_16_131129_create_countries_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `popular_animes`
--

CREATE TABLE `popular_animes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `anime_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `popularity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranked_animes`
--

CREATE TABLE `ranked_animes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `anime_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `top_airing_animes`
--

CREATE TABLE `top_airing_animes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `english_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `japanese_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anime_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `top_airing_animes`
--

INSERT INTO `top_airing_animes` (`id`, `anime_id`, `english_title`, `japanese_title`, `anime_image`, `created_at`, `updated_at`) VALUES
(1, 41467, 'Bleach: Thousand-Year Blood War', 'BLEACH 千年血戦篇', 'https://cdn.myanimelist.net/images/anime/1764/126627l.jpg', NULL, NULL),
(2, 44511, 'Chainsaw Man', 'チェンソーマン', 'https://cdn.myanimelist.net/images/anime/1806/126216l.jpg', NULL, NULL),
(3, 50602, NULL, 'SPY×FAMILY', 'https://cdn.myanimelist.net/images/anime/1111/127508l.jpg', NULL, NULL),
(4, 21, 'One Piece', 'ONE PIECE', 'https://cdn.myanimelist.net/images/anime/6/73245l.jpg', NULL, NULL),
(5, 50172, 'Mob Psycho 100 III', 'モブサイコ100 III', 'https://cdn.myanimelist.net/images/anime/1228/125011l.jpg', NULL, NULL),
(6, 49596, 'Blue Lock', 'ブルーロック', 'https://cdn.myanimelist.net/images/anime/1258/126929l.jpg', NULL, NULL),
(7, 50528, 'Golden Kamuy Season 4', 'ゴールデンカムイ', 'https://cdn.myanimelist.net/images/anime/1855/128059l.jpg', NULL, NULL),
(8, 44042, 'Holo Graffiti', 'ホロのぐらふぃてぃ', 'https://cdn.myanimelist.net/images/anime/1259/110227l.jpg', NULL, NULL),
(9, 47917, 'Bocchi the Rock!', 'ぼっち・ざ・ろっく！', 'https://cdn.myanimelist.net/images/anime/1448/127956l.jpg', NULL, NULL),
(10, 49784, 'Welcome to Demon School! Iruma-kun Season 3', '魔入りました！入間くん', 'https://cdn.myanimelist.net/images/anime/1688/128720l.jpg', NULL, NULL),
(11, 235, 'Case Closed', '名探偵コナン', 'https://cdn.myanimelist.net/images/anime/7/75199l.jpg', NULL, NULL),
(12, 49918, 'My Hero Academia Season 6', '僕のヒーローアカデミア 6th Season', 'https://cdn.myanimelist.net/images/anime/1483/126005l.jpg', NULL, NULL),
(13, 49828, 'Mobile Suit Gundam: The Witch from Mercury', '機動戦士ガンダム 水星の魔女', 'https://cdn.myanimelist.net/images/anime/1440/127624l.jpg', NULL, NULL),
(14, 37822, NULL, '斗罗大陆', 'https://cdn.myanimelist.net/images/anime/1538/93566l.jpg', NULL, NULL),
(15, 49220, 'Uncle from Another World', '異世界おじさん', 'https://cdn.myanimelist.net/images/anime/1743/125204l.jpg', NULL, NULL),
(16, 51805, 'Legend of the Galactic Heroes: Die Neue These - Intrigue', '銀河英雄伝説 Die Neue These 策謀', 'https://cdn.myanimelist.net/images/anime/1700/123762l.jpg', NULL, NULL),
(17, 51039, 'Fights Break Sphere 5th Season', '斗破苍穹年番', 'https://cdn.myanimelist.net/images/anime/1419/126374l.jpg', NULL, NULL),
(18, 966, 'Shin Chan', 'クレヨンしんちゃん', 'https://cdn.myanimelist.net/images/anime/10/59897l.jpg', NULL, NULL),
(19, 49571, 'Swallowed Star', '吞噬星空 壮志凌云', 'https://cdn.myanimelist.net/images/anime/1341/120304l.jpg', NULL, NULL),
(20, 48316, 'The Eminence in Shadow', '陰の実力者になりたくて！', 'https://cdn.myanimelist.net/images/anime/1874/121869l.jpg', NULL, NULL),
(21, 47794, 'MILGRAM', 'ミルグラム', 'https://cdn.myanimelist.net/images/anime/1497/112023l.jpg', NULL, NULL),
(22, 46654, 'IDOLiSH7 Third BEAT! Part 2', 'アイドリッシュセブン', 'https://cdn.myanimelist.net/images/anime/1458/125998l.jpg', NULL, NULL),
(23, 8687, NULL, 'ドラえもん (2005)', 'https://cdn.myanimelist.net/images/anime/6/23935l.jpg', NULL, NULL),
(24, 52193, 'Akiba Maid War', 'アキバ冥途戦争', 'https://cdn.myanimelist.net/images/anime/1119/127385l.jpg', NULL, NULL),
(25, 50552, NULL, '弱虫ペダル LIMIT BREAK', 'https://cdn.myanimelist.net/images/anime/1000/127332l.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_anime_status_lists`
--

CREATE TABLE `user_anime_status_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_favourite_lists`
--

CREATE TABLE `user_favourite_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anime_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_blog_posts`
--
ALTER TABLE `admin_blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animes`
--
ALTER TABLE `animes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_characters`
--
ALTER TABLE `anime_characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_details`
--
ALTER TABLE `anime_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_episodes`
--
ALTER TABLE `anime_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_picutres`
--
ALTER TABLE `anime_picutres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_producers`
--
ALTER TABLE `anime_producers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_recommendations`
--
ALTER TABLE `anime_recommendations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_reviews`
--
ALTER TABLE `anime_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_seaches`
--
ALTER TABLE `anime_seaches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_streamings`
--
ALTER TABLE `anime_streamings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anime_views`
--
ALTER TABLE `anime_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `horror_animes`
--
ALTER TABLE `horror_animes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `popular_animes`
--
ALTER TABLE `popular_animes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranked_animes`
--
ALTER TABLE `ranked_animes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_airing_animes`
--
ALTER TABLE `top_airing_animes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_anime_status_lists`
--
ALTER TABLE `user_anime_status_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_favourite_lists`
--
ALTER TABLE `user_favourite_lists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_blog_posts`
--
ALTER TABLE `admin_blog_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `animes`
--
ALTER TABLE `animes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `anime_characters`
--
ALTER TABLE `anime_characters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `anime_details`
--
ALTER TABLE `anime_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `anime_episodes`
--
ALTER TABLE `anime_episodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `anime_picutres`
--
ALTER TABLE `anime_picutres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `anime_producers`
--
ALTER TABLE `anime_producers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `anime_recommendations`
--
ALTER TABLE `anime_recommendations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `anime_reviews`
--
ALTER TABLE `anime_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `anime_seaches`
--
ALTER TABLE `anime_seaches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `anime_streamings`
--
ALTER TABLE `anime_streamings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `anime_views`
--
ALTER TABLE `anime_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `horror_animes`
--
ALTER TABLE `horror_animes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `popular_animes`
--
ALTER TABLE `popular_animes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ranked_animes`
--
ALTER TABLE `ranked_animes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `top_airing_animes`
--
ALTER TABLE `top_airing_animes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_anime_status_lists`
--
ALTER TABLE `user_anime_status_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_favourite_lists`
--
ALTER TABLE `user_favourite_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
