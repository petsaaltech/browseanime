<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BrowseController;
use App\Http\Controllers\ReviewsController;
use App\Http\Controllers\SiteStatsController;
use App\Http\Controllers\AnimeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserFavouriteListController;
use App\Http\Controllers\MyListController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\UserDetailController;
use App\Http\Controllers\UserAnimeStatusListController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BlogPostController;
use App\Http\Controllers\Admin\CategoryController;

use App\Http\Controllers\SitemapController;

// use Illuminate\Support\Facades\Auth;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/sitemap.xml', [SitemapController::class, 'index']);

// ==========================================

Route::get('/singup', [AuthController::class, 'signup'])->name('signup');
Route::post('/register', [AuthController::class, 'userRegister'])->name('userregister');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'login']);

Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');


Route::get('/dashboard', [AuthController::class, 'dashboard'])->name('dashboard');

Route::get('/add-to-list/{anime_id}/{user_id}/{status}', [UserAnimeStatusListController::class, 'addToList'])->name('addToList');



Route::get('/addtofavlist/{anime_id}/{user_id}', [UserFavouriteListController::class, 'store'])->name('addtofavlist');
Route::get('/removefromfavlist/{anime_id}/{user_id}', [UserFavouriteListController::class, 'removefromfavlist'])->name('removefromfavlist');

Route::get('/addtofavlistsearchitem/{anime_id}/{user_id}', [UserFavouriteListController::class, 'searchItemFav'])->name('addtofavlistsearchitem');


Route::get('/my-list', [MyListController::class, 'index'])->name('my-list');
Route::get('/my-profile', [UserDetailController::class, 'index'])->name('my-profile');

Route::get('/blog-post', [BlogController::class, 'blogpost'])->name('blog-post');
Route::get('/blog-detail/{slug}', [BlogController::class, 'blogpostdetail'])->name('blog-detail');



Route::get('/my-review-list', [UserDetailController::class, 'userReviewList'])->name('userReviewList');
Route::get('/delete-my-review/{id}', [UserDetailController::class, 'deleteUserReview'])->name('deleteUserReview');



Route::get('/user-edit/{user_id}', [UserDetailController::class, 'userEdit'])->name('user-edit');
Route::post('/user-update/{user_id}', [UserDetailController::class, 'userUpdate'])->name('userUpdate');
Route::get('/banner-edit/{user_id}', [UserDetailController::class, 'editBannerAavatar'])->name('editBannerAavatar');
Route::post('/banner-update/{user_id}', [UserDetailController::class, 'updateBannerAvatar'])->name('bannerUpdate');





// ==========================================


Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/redirects', [HomeController::class, 'redirects'])->name('redirects');
Route::get('/detail/{id}', [HomeController::class, 'animeDetailV2'])->name('animeDetail');

Route::get('/searchanime', [HomeController::class, 'animeSearch'])->name('searchanime');
Route::post('/search', [HomeController::class, 'searchAnime'])->name('search');

Route::get('/browse', [BrowseController::class, 'index'])->name('browse');
Route::get('/images', [BrowseController::class, 'randomImages'])->name('randomImages');

Route::get('/reviews', [ReviewsController::class, 'index'])->name('reviews');
Route::get('/review-detail/{id}', [ReviewsController::class, 'reviewDetail'])->name('reviewDeail');
Route::get('/review-add/{anime_id}', [ReviewsController::class, 'reviewAdd'])->name('reviewAdd');
Route::post('/review-store/{anime_id}/{user_id}/{user_name}', [ReviewsController::class, 'reviewStore'])->name('reviewStore');

Route::get('/anime-reviews/{anime_id}', [ReviewsController::class, 'animeReviewListing'])->name('animeReviewsListing');



Route::get('/site-stats', [SiteStatsController::class, 'index'])->name('site-stats');


// ==========================================


Route::get('/insertPopularAnime/{id}', [AnimeController::class, 'popularAnimeInsert'])->name('insertPopularAnime');
Route::get('/insertHorrorAnime/{id}', [AnimeController::class, 'horrorAnimeInsert'])->name('insertHorrorAnime');
Route::get('/insertRankedAnime/{id}', [AnimeController::class, 'rankedAnimeInsert'])->name('insertRankedAnime');
Route::get('/insert-weekly-top', [AnimeController::class, 'topAiringAnimeInsert'])->name('topAiringAnimeInsert');

// ==========================================


// admin dashboard

Route::get('/admin/login', [AdminController::class, 'loginIndex']); 

// Route::post('logout', [AdminController::class, 'logout'])->name('logout');

Route::post('/admin/login', [AdminController::class, 'login'])->name('admin-login');

Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('adminDashboar'); 


Route::get('/admin/blog', [BlogPostController::class, 'adminBlog'])->name('adminBlog'); 



// Route::resource('/admin/adminblogpostcategories/', CategoryController::class);
// Route::resource('category', CategoryController::class);
Route::resource('admin/adminblogcategory', CategoryController::class);
Route::resource('admin/adminblogpost', BlogPostController::class);
// ->names([
//     'index' => "blog.index"
// ]);

// Route::resource('/admin/adminblogpostcategories/', AdminBlogPostCategoryController::class);



    // Route::get('/admin/adminblogcategory', [AdminBlogCategoryController::class,'index'])->name('adminblogcategory');
    // Route::post('/admin/adminblogcategorystore', [AdminBlogCategoryController::class,'store'])->name('adminblogcategory');
    
    // Auth::routes();
// Route::group(['middleware'=>'auth'],function () {

//     // Route::get('/admin/adminblogcategory', [AdminBlogCategoryController::class,'index'])->name('adminblogcategory');
//     // Route::post('/admin/adminblogcategory', [AdminBlogCategoryController::class,'store'])->name('adminblogcategory');
    
// });






